-- [Hk]Config++ by Hekkaryk
-- Hello ;) Good day/dusk/night to you ^_^
-- All global variables are intentional. I see no point in re-defining same variables in tons of files when others mods have no access to variables defined by mod (as of 2017-11-18).
-- Hurr durr, GLOBAL.pcall, such safety, much wow, try-catch all UwU~ >:D
local _, error = GLOBAL.pcall(function()
    _G = GLOBAL
    require = _G.require
    _TUNING = _G.TUNING
    _STRINGS = _G.STRINGS
    _RECIPETABS = _G.RECIPETABS
    _TECH = _G.TECH
    _Ingredient = _G.Ingredient
    _KnownModIndex = _G.KnownModIndex

    IsServer = _G.TheNet:GetIsServer()
    io = require "io"
    os = require "os"
    -- What mod are loaded - from Rezecib's Rebalance
    local function is_mod_loaded(workshop_name)
        if _KnownModIndex:IsModEnabled(workshop_name) then
            return true
        end
        for _, moddir in ipairs(_KnownModIndex:GetModsToLoad()) do
            if moddir == workshop_name then
                return true
            end
        end
        return false
    end

    -- This global variables stores information about which mods are loaded. Do not prepend next line with local or mod will always crash.
    loaded_mods = {}
    -- Multi-World DST https://steamcommunity.com/sharedfiles/filedetails/?id=726432903 + it's patched version
    loaded_mods["Multi-Worlds DST"] = is_mod_loaded("workshop-726432903") or is_mod_loaded("workshop-1237378537")
    -- My Birds and Berries [Megarandom] https://steamcommunity.com/sharedfiles/filedetails/?id=1079880641
    loaded_mods["My Birds and Berries [Megarandom]"] = is_mod_loaded("workshop-1079880641")
    -- Birds and Berries and Trees and Flowers for Friends https://steamcommunity.com/sharedfiles/filedetails/?id=522117250
    loaded_mods["Birds and Berries and Trees and Flowers for Friends"] = is_mod_loaded("workshop-522117250")
    -- DST Advanced Farming https://steamcommunity.com/sharedfiles/filedetails/?id=370373189
    loaded_mods["DST Advanced Farming"] = is_mod_loaded("workshop-370373189")
    -- Wind Goddess Magic https://steamcommunity.com/sharedfiles/filedetails/?id=758789422
    loaded_mods["Wind Goddess Magic"] = is_mod_loaded("workshop-758789422")
    -- Tea and Trees https://steamcommunity.com/sharedfiles/filedetails/?id=912221515
    loaded_mods["Tea and Trees"] = is_mod_loaded("workshop-912221515")
    -- Red Lycoris(support DS&DST) https://steamcommunity.com/sharedfiles/filedetails/?id=1205587210
    loaded_mods["Red Lycoris(support DS&DST)"] = is_mod_loaded("workshop-1205587210")
    -- Tea and Trees https://steamcommunity.com/sharedfiles/filedetails/?id=912221515
    loaded_mods["Tea and Trees"] = is_mod_loaded("workshop-912221515")

    -- [Hk]Config++
    loaded_mods["[Hk]Config++"] = is_mod_loaded("workshop-655377781")
    -- [Hk]Config++ [DEV]
    loaded_mods["[Hk]Config++ [DEV]"] = is_mod_loaded("workshop-960342576")
    -- [Hk]Config++ [PREV]
    loaded_mods["[Hk]Config++ [PREV]"] = is_mod_loaded("workshop-965531598")

    local hk_already_loaded = 0
    if loaded_mods["[Hk]Config++"] then
        hk_already_loaded = hk_already_loaded + 1
    end
    if loaded_mods["[Hk]Config++ [DEV]"] then
        hk_already_loaded = hk_already_loaded + 1
    end
    if loaded_mods["[Hk]Config++ [PREV]"] then
        hk_already_loaded = hk_already_loaded + 1
    end

    if hk_already_loaded > 1 then
        print(
            "[Hk]Config++ FATAL: It's impossible to run several versions of same mod in parallel. This version of mod halts execution due to invalid server configuration.")
    else
        modimport("init/utils")

        modimport("init/enums")

        -- This is how I load all config fiels. Yes really. No, I'm not going to use GetModConfigData for that; I have too many fields and too much code in other files.
        -- Stores mod configuration (key-value collection)
        config = {}

        local function load_config(modname)
            local config_values, temp_options = _KnownModIndex:GetModConfigurationOptions_Internal(modname, false)
            if config_values and type(config_values) == "table" then
                if temp_options then
                    return config_values
                else
                    local result = {}
                    for _, v in pairs(config_values) do
                        if v.saved ~= nil then
                            result[v.name] = v.saved
                        else
                            result[v.name] = v.default
                        end
                    end
                    return result
                end
            else
                print("[Hk]Config++: Failed to read mod configuration! Mod will not start up.")
            end
        end

        local result, error = GLOBAL.pcall(function()
            config = load_config(modname)
        end)

        if error ~= nil then
            print("[Hk]Config++: Could not load config! Update load_config funtion! Error:" .. cast_to_string(error))
        end

        -- if mod is disabled do not turn it on
        if config["[Hk]Config++"] == true then
            -- tools, weapons & armors durability
            modimport("init/retune")

            -- recipes
            modimport("init/recipes")

            -- quickpick
            modimport("init/quickpick") -- consider changing config field quick_pick into enable_quickpick then using enable("quickpick")

            -- stack size
            modimport("init/stack_size")

            -- no fire spread
            modimport("init/fire_spread") -- consider changing config field no_fire_spread to enable_nofirespread and changing fire_spread.lua into nofirespread.lua then using enable("nofirespread")

            -- thermal stones
            -- modimport("init/heatrocks") -- bugged af

            -- telltale heart
            modimport("init/telltales") -- consider changing config field cheap_telltales to enable_cheaptelltales and changing telltales.lua into cheaptelltales.lua then using enable("cheaptelltales")

            -- fast working
            modimport("init/fast_working")

            -- stackable animals
            modimport("init/stackables")

            -- Ancient_Guardian respawn
            modimport("init/minotaur")

            -- Ancient Fuelweaver overworld spawning
            enable("overworld_fuelweaver")

            -- custom commands
            enable("commands")

            enable("special_events")

            modimport("init/structures_protection")

            enable("bugfixes")

            modimport("init/itemchanges")

            enable("cleanup")

            enable("observer")

            enable("container_logger")

            if config.fastbuilder or config.banned_characters ~= "" or config.enable_commands or
                config.enable_starting_items or config.custom_starting_items or config.enable_starting_items_seasonal or
                config.enable_starting_items_night or config.enable_starting_items_cave or
                config.enable_starting_items_pvp then
                modimport("init/hk_playerjoined")
            end

            -- or config.reveal_map ~= "no"
            if config.enable_starting_items or config.custom_starting_items or config.enable_starting_items_seasonal or
                config.enable_starting_items_night or config.enable_starting_items_cave or
                config.enable_starting_items_pvp then
                modimport("init/hk_playerspawn")
            end

            if config.disable_wildfires then
                _TUNING.WILDFIRE_CHANCE = 0
                _TUNING.WILDFIRE_THRESHOLD = 100
                log_i("Disabling wildfires...")
            end

            if config.enable_pickup or (config.enable_cmd_save and config.enable_cmd_save < 5) then
                local function HkOnSave(inst, data)
                    if inst.HkPrevOnSave ~= nil then
                        inst.HkPrevOnSave(inst, data)
                    end
                    if inst.hkdata ~= nil then
                        data.hkdata = inst.hkdata
                    end
                end

                local function HkOnLoad(inst, data)
                    if inst.HkPrevOnLoad ~= nil then
                        inst.HkPrevOnLoad(inst, data)
                    end
                    if data ~= nil then
                        if data.hkdata then
                            inst.hkdata = data.hkdata
                        else
                            inst.hkdata = {}
                            inst.hkdata.guid = get_long_random_string()
                        end
                    end
                end

                AddPrefabPostInitAny(function(inst) -- yep. ALL OF 'EM. EVERY. SINGLE. ITEM.
                    inst.hkdata = {}
                    inst.hkdata.guid = get_long_random_string()
                    inst.HkPrevOnSave = inst.OnSave
                    inst.OnSave = HkOnSave
                    inst.HkPrevOnLoad = inst.OnLoad
                    inst.OnLoad = HkOnLoad
                end)
            end

            enable("pickup")

            if config.banned_prefabs ~= "" then
                modimport("init/banned_prefabs")
            end

            if config.minimum_days_to_open_container ~= nil and config.minimum_days_to_open_container > 0 then
                modimport("init/container_open_restriction")
            end

            -- if config.reveal_map ~= "no" then
            --     modimport("init/hk_playeractivated")
            -- end

            if config.resources_respawn_delay and type(config.resources_respawn_delay) == "number" and
                config.resources_respawn_delay > 0 then
                modimport("init/resources_respawn")
            end

            non_player_structures = {} -- reused in init/environment_protection.hk, init/explosives_control.lua and structures_protection.lua

            if nil_or_empty(config.non_player_structures) then
                config.non_player_structures = "beehive,houndmound,mermhead,pighead,pighouse,rabbithouse,spiderden,wasphive"
            end

            for _, v in first_level_string_split(config.non_player_structures) do
                non_player_structures.insert(v, true)
            end

            enable("explosives_control")

            if not_nil_or_empty(config.no_burning_days) and config.no_burning_days > 0 then
                modimport("init/environment_protection")
            end

            log_i("End of configuration phase.")
        elseif config["[Hk]Config++"] == false then
            log_f("Mod is disabled via config option!")
        end
    end
end)

if error ~= nil then
    print("[Hk]Config++ FATAL ERROR - LOADING MODMAIN FAILED! Error message: " .. error)
end
