-- Mod by Hekkaryk. Hope you have a nice day! ;)
author = "Hekkaryk"
version = "8.2.5"
name = "[Hk]Config++"
description = "v" .. version ..
                  "\nThis mod allows you to change unavailable settings, add recipes, modify number of uses or properties of certain in-game objects."
forumthread = ""
api_version = 10
dst_compatible = true
dont_starve_compatible = false
reign_of_giants_compatible = false
all_clients_require_mod = true
client_only_mod = false
server_filter_tags = {
    "fast travel",
    "custom commands",
    "quick pick",
    "starting items",
    "increased stack size",
    "craftable gears",
    "no perish iceboxes",
    "Multi-World DST compatible",
    "respawnable AG",
    "spawnable overworld AF",
    "no respawn penalty",
    "enchanced durability",
    "fast chopping",
    "fast mining",
    "craftable animals",
    "no fire spread",
    "tuning"
}

-- configuration field
local function field(name, label, hover, options, default)
    return {
        name = name,
        label = label,
        hover = hover,
        options = options,
        default = default
    }
end

-- configuration field's options
local function option(_description, _data)
    if _description ~= nil and _data ~= nil then
        return {
            description = _description,
            data = _data
        }
    end
end

local function option_n(value)
    return option(value, value + 0) -- There is no "GLOBAL" here, and "tonumber(string)" is nil.
end

local function option_s(value)
    return option(value, value)
end

local function option_h(_description, _data, _hover)
    if _description ~= nil and _hover ~= nil and _data ~= nil then
        return {
            description = _description,
            hover = _hover,
            data = _data
        }
    end
end

-- returns set of options from min to max by step - skips overhead if it's not reached in step (last returned option will always be below max)
local function options_range(min, max, step)
    local array = {}
    local index = 1
    for i = min, max, step do
        array[index] = option_n(i)
        index = index + 1
    end
    return array
end

local function merge_tables(first, second)
    local orig_size = #first
    for i = 1, #second do
        first[orig_size + i] = second[i]
    end
    return first
end

local options_timezone = merge_tables(merge_tables({
    option_h("Local", -13 + 0, "Use local timezone"),
    option_h("UTC+0", 0 + 0, "Coordinated Universal Time, an worldwide standard timezone")
}, options_range(-12, -0.5, 0.5)), options_range(0.5, 12, 0.5))
-- note for dedicated servers administrators: you can define and use your own datetimeformat as long as it's Lua-compatible
local options_datetimeformat = {
    option_h("none", "none", "Do not display date and time"),
    option_h("default", "%Y-%m-%d %H:%M:%S", "YYYY-mm-DD HH:MM:SS"),
    option("YYYY-mm-DD-HH-MM-SS", "%Y-%m-%d-%H-%M-%S"),
    option("DD.mm.YYYY HH:MM:SS", "%d.%m.%Y %H:%M:%S"),
    option("DD.mm.YYYY.HH.MM.SS", "%d.%m.%Y.%H.%M.%S")
}

local options_log_level = {
    option_h("none", 0, "unrecommended - no info in case of problems"),
    option("fatal", 1),
    option("error", 2),
    option("warning", 3),
    option("info", 4),
    option("log", 5),
    option("debug", 6)
}

local options_bool = {
    option_h("no", false, "vanilla game behaviour"),
    option_h("yes", true, "modded behaviour")
}

local options_bool_simple = {
    option("no", false),
    option("yes", true)
}

local options_difficulty = {
    option_h("no", 0, "disable"),
    option("easy", 1),
    option("medium", 2),
    option("hard", 3)
}

local options_gathering = {
    option("normal", 1),
    option("instant", 2),
    option_h("faster", 3, "axe/pickaxe uses per hit will be increased to make it fair, resource-wise")
}

local options_stackable_animals = {
    option("no", 0),
    option_h("all", 7, "rabits, birds and moleworms"),
    option_h("1+2", 3, "rabbits and birds"),
    option_h("1+3", 5, "rabbits and moleworms"),
    option_h("2+3", 6, "birds and moleworms"),
    option_h("rabbits", 1, "rabbits"),
    option_h("birds", 2, "birds"),
    option_h("moleworms", 4, "moleworms")
}

local options_durability = {
    option_h("normal", 1, "normal"),
    option_n("2"),
    option_n("5"),
    option_n("10"),
    option_n("25"),
    option_n("50"),
    option_n("100"),
    option_h("infinity", 0, "WILL break long games")
}

local options_regrow_rate = {
    option_n("0.25"),
    option_n("0.33"),
    option_n("0.5"),
    option_n("0.66"),
    option_n("0.75"),
    option_h("1", 1, "normal"),
    option_h("2", 2, "twice as long as in vanilla game!"),
    option_h("3", 3, "thrice (!) as long as in vanilla game!")
}

local options_berries_cycles = {
    option_h("normal", 3, "vanilla values"),
    option_n("5"),
    option_n("10"),
    option_n("15")
}

local options_eyeturrets_strength = {
    option_h("normal", 0, "normal"),
    option("strong", 1),
    option("stronger", 2),
    option("strongest", 3)
}

local options_commands = {
    option("off", 6),
    option("Admins", 5),
    option("Elite Mods+",4),
    option("Mods+", 3),
    option("Elite+", 2),
    option("Prestige+", 1),
    option("all", 0)
}

local options_cmd_portal_target = {
    option("Portal", "multiplayer_portal, multiplayer_portal_moonrock, multiplayer_portal_moonrock_constr"),
    option("Oasis Lake", "oasislake"),
    option("Moon Stone", "moonbase")
}

local options_coords_per_rank = {
    option("none", -1),
    option("infinity", 0)
}
options_coords_per_rank = merge_tables(options_coords_per_rank, options_range(1, 10, 1))

local options_structures = {
    option("none", -1),
    option_h("APS+scaled", 1, "Ancient Pseudoscience Station and Scaled Chests"),
    option_h("+iceboxes", 2, "previous + iceboxes")
}

local options_chests_protection_selection = {
    option("none", 0),
    option_h("scaled", 1, "Scaled chests only"),
    option_h("all", 2, "ALL chests - NO CONTENT WILL BE DROPPED WHEN DESTROYED BY MONSTER TOO!")
}

local options_chests_protection_strategy = {
    option_h("drop", 0, "Containers should yield their content when destroyed"),
    option_h("drop if burnt", 1, "Containers should yield their content - but only if it was burned"),
    option_h("don't drop", 2, "No content drop in any scenario")
}

local options_chests_protection_durability = {
    option("original", 0)
}
options_chests_protection_durability = merge_tables(options_chests_protection_durability, options_range(2, 150, 1))

local options_banned_characters = {
    option("none", ""),
    option_h("td1madao", "deidara2_dst_td1madao, riven2_dst_td1madao, yorha2b_dst_td1madao", "Deidara, Riven and 2B"),
    option_h("Deidara", "deidara2_dst_td1madao", "Deidara"),
    option_h("Riven", "riven2_dst_td1madao", "Riven"),
    option_h("2B", "yorha2b_dst_td1madao", "2B"),
    option_h("strong", "wolfgang,wx78,wathgrihr", "Wolfgang, WX-78 and Wigfrid"),
    option_h("useful", "wickerbottom,waxwell", "Wickerbottom and Maxwell"),
    option_h("weak", "willow,wendy,wes", "Willow, Wendy and Wes"),
    option_h("mediocre", "wilson,woodie,webber,winona", "Wilson, Woodie, Webber and Winona"),
    option("Wilson", "wilson"),
    option("Willow", "willow"),
    option("Wolfgang", "wolfgang"),
    option("Wendy", "wendy"),
    option("WX-78", "wx78"),
    option("Wickerbottom", "wickerbottom"),
    option("Woodie", "woodie"),
    option("Wes", "wes"),
    option("Maxwell", "waxwell"),
    option("Wigfrid", "wathgrithr"),
    option("Webber", "webber"),
    option("Winona", "winona")
}

local options_banned_characters_message = {
    option_s("Character you picked is banned! Despawning in 5 seconds..."),
    option_s("Character you picked is banned! Change banned_characters settings to play as this character")
}

local options_banned_prefabs = {
    option("None", ""),
    option("Deconstruction Staffs", "greenstaff"),
    option("Torches", "torch")
}

local default_ranks = "prestige,elite,mod,elitemod"

configuration_options = {
    field("quick_pick", "Quick pick", "Use quick pick for everything?", options_bool, true),
    field("stack_size", "Stack size",
          "ATTENTION PLEASE: Following this update DO NOT change stack size between <=250 and >250 mid game (WILL break it)",
          options_range(0, 65535, 50), 250),
    field("food_perish_rate", "Frozen food perish",
          "How fast should food/animal perish when in icebox/icepack?\nDefault is 0.5, set to 0 to stop food/animal perish when stored in fridge.",
          options_range(0, 2, 0.1), 0),
    field("no_respawn_penalty", "No respawn penalty", "Remove max health penalty on respawn?", options_bool, true),
    field("no_fire_spread", "Fire spread", "Disable fire spread? (does not affect Star Caller Star's fires!)",
          options_bool, true),
    field("[Hk]Config++", "Enable mod", "You have to set it to yes to enable this mod.", options_bool, true),
    field("timezone", "Timezone", "Which timezone would you like to see in logs?", options_timezone, -13),
    field("datetimeformat", "Date and time format", "How would you like date and time to be displayed in logs?",
          options_datetimeformat, "%Y-%m-%d %H:%M:%S"),
    field("log_level", "Log level", "How much info would you like to see in server logs", options_log_level, 5),
    field("free_hammering", "Free hammering", "Make hammered down structures yield their full cost?", options_bool,
          false),
    -- field("permanent_heatrock", "Permanent thermal stones", "Make thermal stones permanent?", options_bool, false),
    field("cheap_telltale", "Cheap Telltale Heart", "Remove health cost from Tellatale Heart recipe?", options_bool,
          true),
    field("ratio_tools", "Tools uses multiplier", "Multiply original number of uses by selected", options_durability, 1),
    field("ratio_weapons", "Weapons uses multiplier", "Multiply original number of uses by selected",
          options_durability, 1),
    field("ratio_staffs", "Staffs uses multiplier", "Multiply original number of uses by selected", options_durability,
          1),
    field("ratio_amulets", "Amulets uses multiplier", "Multiply original number of uses by selected",
          options_durability, 1),
    field("ratio_clothes", "Clothes durability multiplier", "Multiply original durability by selected",
          options_durability, 1),
    field("ratio_armors", "Armors durability multiplier", "Multiply original durability by selected",
          options_durability, 1),
    field("ratio_light", "Light sources durability multiplier", "Multiply original length by selected",
          options_durability, 1),
    field("ratio_boss_hp", "Boss HP multiplier", "Multiply original length by selected", options_range(0.25, 8, 0.25), 1),
    field("speed_chopping", "Tree chopping", "Speeds up chopping trees", options_gathering, 1),
    field("speed_mining", "Mining rocks", "Speeds up mining", options_gathering, 1),
    field("stackable_animals", "Stackable animals", "Enables stackable rabbits[1], birds[2] andor moleworms[3]",
          options_stackable_animals, 0),
    field("recipes_monster_drops", "Craftable monster drops", "Add monster drops recipes", options_difficulty, 0),
    field("recipes_plants", "Craftable plants", "Add plant recipes", options_difficulty, 0),
    field("recipes_animals", "Craftable animals",
          "Add rabbits, bees, moleworms and birds recipes (WARNING: killing yield more that is used to craft!)",
          options_difficulty, 0),
    field("recipes_common", "Craftable common items",
          "Add boneshards, marble, lightbulb, fireflies, honeycomb and hound tooth recipes", options_difficulty, 0),
    field("recipes_spawners", "Craftable spawning items", "Add lure plant and spider eggs recipes", options_difficulty,
          0),
    field("recipes_rare", "Craftable rare items", "Add Mandrake, Krampus Sack, Spider Hat, Walrus Tusk and Hat recipes",
          options_difficulty, 0),
    field("recipes_combat", "Craftable combat items", "Add Battle Spear, Battle Helm and Tentacle Spike recipes",
          options_difficulty, 0),
    field("recipes_caves", "Craftable cave items", "Add Thulecite Fragments, Slurper Pelt and Bunnyman tail recipes",
          options_difficulty, 0),
    field("recipes_thulecite", "Craftable Thulecite",
          "Change original Thulecite recipe - Thulecite Fragments will be useless!", options_difficulty, 0),
    field("recipes_chess", "Craftable chess piece sketches", "Add all figures sketch recipes (no icons!) recipes",
          options_difficulty, 0),
    field("recipes_multiworld", "Multi-World DST recipes", "Add Multi-World DST mod item recipes?", options_difficulty,
          0),
    field("recipes_berries_and_plants", "Berries and plants recipes", "Add Berries and plants mod item recipes?",
          options_difficulty, 0),
    field("recipes_tea_and_trees", "Tea and Trees recipes", "Add Tea and Trees mod item recipes", options_difficulty, 0),
    field("recipes_bulk_craft", "Bulk craft recipes", "Add recipes allowing faster crafting?", options_bool, false),
    field("minotaur_respawn", "Ancient Guardian respawn", "Enable Ancient Guardian respawn?", options_bool, false),
    field("enable_overworld_fuelweaver", "Overworld Fuelweaver", "Allow Ancient Fuelweaver to be summoned in overworld",
          options_bool, false),
    field("fastbuilder", "Fast crafting", "Give everybody fastbuilder perk?", options_bool, true),
    field("no_grass_geckos", "No Grass Gecko spawn", "Deny spawning Grass Geckos from grass?", options_bool, true),
    field("regrow_rate", "Regrow rate", "Rate of plants yields (less==faster)", options_regrow_rate, 1),
    field("berries_cycles", "Berries cycles",
          "How many times berries might be gathered before they need to be re-fertilized?", options_berries_cycles, 3),
    field("stronger_eyeturrets", "Eyeturrets strength", "Eyeturrets strength rate", options_eyeturrets_strength, 1),
    field("no_blight", "Stop plague", "Stop spread of disease - even in worlds with blight on?", options_bool, true),
    field("add_atrium_turret", "Add Atrium Turret", 'c_spawn("atrium_turret") and c_spawn("atrium_turret_item") only',
          options_bool, true),
    field("add_herder_turret", "Add Herded Turret", "FIXED! Beefalo-friendly Atrium Turret", options_bool, false),
    field("enable_special_events", "Enables all events",
          "Note: this requires events to be NOT set to Auto in world creation", options_bool, false),
    field("enable_commands", "Enable commands", "Setting this to true will enable commands", options_bool, true),
    field("enable_cmd_help", "Enable help", "/cmd_help shows available commands", options_commands, 0),
    field("enable_cmd_info", "Enable info", "/info name shows information about player", options_commands, 0),
    field("enable_cmd_revive", "Enable revive", "/r, /re and /revive revive on spot", options_commands, 0),
    field("command_revive_penalty", "Command revive penalty",
          "How much health should player lose when revived via command?", options_range(0, 0.75, 0.05), 0.1),
    field("enable_cmd_portal", "Enable portal fast-travel", "/q, /esc, /portal teleports to portal", options_commands, 0),
    field("cmd_portal_target", "Portal prefab", "Where should /portal take players", options_cmd_portal_target,
          "multiplayer_portal, multiplayer_portal_moonrock, multiplayer_portal_moonrock_constr"),
    field("enable_cmd_return", "Enable return", "/mark (and /q) marks spot, return travel to it", options_commands, 0),
    field("enable_cmd_save", "Enable save/load/delete",
          "/save n saves place you can travel back with /load n; /delete n removes it - /set n saves public spot, /get n and /unset\nload without parameter to see available destinations - shortcuts are: /+ /= /- /-all /++ /== /-- /--all",
          options_commands, 0),
    field("coords_per_rank", "Coords per rank", "How many coordinates can players save (value + rank*value)",
          options_coords_per_rank, 4),
    field("enable_cmd_ext", "Enable fire extinguish", "/e, /ext, /extinguish puts out fires", options_commands, 2),
    field("enable_cmd_invite", "Enable invites",
          "/invite player - allow player to join you with /join player\n/deny player un-invites the player",
          options_commands, 1),
    field("enable_cmd_tp", "Enable fast-travel to players", "/tp player_name warps to player", options_commands, 3),
    field("enable_cmd_stasis", "Enable stasis",
          "/stasis, /hibernate puts player to sleep while making them immune to direct damage - if player will cheat to get out of it they will fall asleep second later",
          options_commands, 1),
    field("enable_cmd_warp", "Enable precise teleportation",
          "/warp, /teleport_to x y z teleports to x y z (in-game coords)", options_commands, 3),
    field("enable_cmd_rank", "Enable ranks", "/rank mod|elite|prestige add|del x to modify rank of x", options_commands,
          3),
    --     field("cmd_rank_names", "Rank names", "Setup rank names", options_cmd_rank_names,
    --           "Prestige, Moderator (vacationing), Moderator"),
    field("enable_cmd_reviveall", "Enable mass revive", "/reviveall restores everybody to life", options_commands, 3),
    field("enable_cmd_resetall", "Enable all files reset",
          "same as rm -rf for mod data - /resetall deletes all ranks/coords files (do not try this at prod server without backups)",
          options_commands, 4),
    field("enable_cmd_reset", "Enable specific file reset",
          "allows resetting rank or coord or player file - /reset for options or /reset hk_config_ + players|mod|elite|prestige|coords_master_private|coords_master_public|coords_caves_private|coords_caves_public|invites",
          options_commands, 4),
    field("enable_cmd_revive_other", "Enable remote revive", "/revive player_name revives player", options_commands, 3),
    field("enable_cmd_pull", "Enable pulling people to ourselves", "/pull x teleports x to us, /push x sends them back",
          options_commands, 3),
    field("enable_cmd_freeze", "Enable freeze",
          "/freeze, /stop x pins down x in a single spot, /unfreeze, /release x frees criminal scum", options_commands,
          3),
    field("enable_cmd_hide", "Enable invisibility", "/hide, /cloak turns on/off invisibility", options_commands, 3),
    field("enable_cmd_kick", "Enable kick", "/hk_kick x or /kick2 x kicks player x out of game", options_commands, 3),
    field("enable_cmd_drop", "Enable drop",
          "/drop x makes x drop all their items - works with everything and also allow removal of corrupted items occupying slots",
          options_commands, 3),
    field("enable_cmd_filecheck", "Enable file checking", "Mod require initialization by executing /file, /check once",
          options_commands, 4),
    field("enable_cmd_tgm", "Enable god mode", "/tgm toggles god mode", options_commands, 4),
    field("enable_cmd_ban", "Enable banning", "/hk_ban x or /ban2 x bans x, /hk_unban x or /unban2 unbans x",
          options_commands, 3),
    field("enable_cmd_die", "Enable suicide", "/die kills player", options_commands, 0),
    field("enable_cmd_kill", "Enable killing", "/kill x kills x (Moderators and above are protected)", options_commands,
          3),
    field("enable_cmd_give", "Enable give", "/give x amount gives player amount of x", options_commands, 3),
    field("enable_cmd_spawn", "Enable spawn", "/spawn prefab_name amount spawns specified prefab at player",
          options_commands, 3),
    field("cmd_step_prefabs", "Spawning step size",
          "How many prefabs can be spawned at once (more than 100 cause noticeable lag)", options_range(10, 500, 10),
          100),
    field("cmd_step_seconds", "Spawning step time",
          "How long should game wait before spawning next batch of prefabs\n(the less spawned a step the shorter it can be; too long might cause weird game behaviour)",
          options_range(1, 20, 1), 2),
    field("enable_cmd_repick_character", "Enable restart", "/repick_character restarts game for player who write it",
          options_commands, 0),
    field("enable_cmd_alert", "Enable alert", "/alert MESSAGE announces MESSAGE to all players", options_commands, 2),
    field("enable_cmd_creative", "Enable creative",
          "/creative name to toggle creative mode for target (skip name to do it to yourself)", options_commands, 4),
    field("enable_cmd_add_points", "Allows adding Achievement mod points",
          "/add_points [how_much] [player_name] adds [how_much] points to [player_name] (which can contains up to 8 spaces)",
          options_commands, 3),
    field("resistant_structures", "Tough structures", "Make selected structures deconstructible only",
          options_structures, 1),
    field("chests_protection_selection", "Protected chests", "Which containers should be protected?",
          options_chests_protection_selection, 1),
    field("chests_protection_strategy", "Destroy policy", "Should container drop its content when destroyed?",
          options_chests_protection_strategy, 2),
    field("chests_protection_durability", "Chests durability", "Number of hits it take to destroy container",
          options_chests_protection_durability, 10),
    field("enable_bugfixes", "Enable bugfixes", "Fix issue with blueprints dropped from chests crash loop?",
          options_bool, true),
    field("orangeamulet_pick_stacks", "Wise Forager", "Make Lazy Forager pick up stacks", options_bool, false),
    field("orangeamulet_icd", "Forager speed", "Cooldown between Lazy Forager item pickup",
          options_range(0.01, 1, 0.01), 0.33),
    field("orangeamulet_range", "Forager range", "Lazy Forager range", options_range(1, 1000, 1), 4),
    field("enable_cleanup", "Enable cleanup",
          "Should cleanup (periodic removal of items exceeding set limits) be enabled?", options_bool, false),
    field("cleanup_cooldown", "Cleanup cooldown", "How much time should pass between cleanups?",
          options_range(60, 10800, 10), 60),
    field("cleanup_warning", "Cleanup warning", "Display warning x seconds before cleanup", options_range(10, 300, 10),
          10),
    field("cleanup_ignore_domesticated", "Cleanup ignore domesticated",
          "Should domesticated creatures be ignored by cleanup?", options_bool, true),
    field("enable_observer", "Observe kills", "Add kills logging/announcing?", options_bool, false),
    field("add_atrium_icebox", "Add Atrium Icebox", "Add indestructible Icebox?", options_bool, false),
    field("disable_wildfires", "Disable wildfires", "Remove wildfires in existing worlds", options_bool, true),
    field("banned_characters", "Banned characters", "Selected characters will be unpickable", options_banned_characters,
          "deidara2_dst_td1madao, riven2_dst_td1madao, yorha2b_dst_td1madao"),
    field("banned_characters_popup_delay", "Time bofore warning",
          "How much time should pass before warning is displayed. This will also delay total time before despawn!",
          options_range(1, 120, 1), 5),
    field("banned_characters_timeout", "Time till respawn",
          "How much time people have before they are respawned via banned character despawn", options_range(1, 120, 1),
          5),
    field("banned_characters_message", "Forced repick message",
          "What message should be displayed to people when they join as banned character",
          options_banned_characters_message, "Character you picked is banned! Despawning in 5 seconds..."),
    field("banned_character_item_drop", "Inventory drop on forced repick",
          "Should character drop their stuff on forced respawn", options_bool_simple, false),
    field("enable_pickup", "Log item IDs on pickup",
          "Should custom, unique ID should be logged when somebody pickups something?", options_bool, true),
    field("enable_rank_badges", "Enable moderators badge", "Should moderators rank be displayed in TAB player list?",
          options_bool, true),
    field("banned_prefabs", "Disable crafting specified prefabs",
          "Prefabs listed in this setting will be impossible to craft/despawn on PostInit (in dedicated server setup split banned prefabs with comma sign).",
          options_banned_prefabs, ""),
    field("minimum_days_to_open_container", "Container access restriction",
          "Defines how many days player has to play to be allowed to open container", options_range(0, 10, 1), 0),
    field("enable_starting_items", "Starting items", "Give new players starting items", options_bool, false),
    field("custom_starting_items", "Custom starting items",
          "Turns off default modded starting items and allow you to define your own; must have format prefab:count,prefab2:count2",
          {
        option("none", ""),
        option_h("basic", "cutgrass:10,twigs:10,flint:5,log:5,goldnugget:1",
                 "10 cut grass, 10 twigs, 5 flint, 5 log and 1 gold nugget")
    }, ""),
    field("enable_starting_items_seasonal", "Seasonal starting items",
          "Give new players starting items reflecting current season", options_bool, false),
    field("enable_starting_items_night", "Night starting items", "Give new players torch if they start at night",
          options_bool, false),
    field("enable_starting_items_cave", "Cave starting items",
          "Give new players starting equipment helpful in exploring caves", options_bool, false),
    field("enable_starting_items_pvp", "PVP starting items", "Give new players starting PVP equipment", options_bool,
          false),
    field("custom_tuning", "TUNING reconfiguration",
          "This settings allows you to change specific TUNING values (that control various game constants)", {
        option_h("none", "", "No extra TUNING changes"),
        option("weak Dragonfly", "DRAGONFLY_HEALTH:5500,DRAGONFLY_BREAKOFF_DAMAGE:1000,DRAGONFLY_STUN_DURATION:5"),
        option("weak ghosts", "SANITY_GHOST_PLAYER_DRAIN:0")
    }, ""), -- other notable tunings: DRY_FAST, DRY_MED, FARM1_GROW_BONUS, FARM2_GROW_BONUS, FARM3_GROW_BONUS
    field("non_player_structures", "NP structures", "List of non-player structures", {
        option("standard", "")
    }, ""),
    field("enable_explosives_control", "Safe explosions",
          "Prevents explosions from destroying/setting on fire buildings",
          options_bool, false),
    field("no_burning_days", "Days to burn", "Defines how long character are denied setting things ablaze",
          options_range(0, 10, 1), 0),
    field("no_destroying_days", "Days to destroy", "Defines how long character are denied destroying things",
          options_range(0, 10, 1), 0),
    field("resources_respawn_delay", "Custom respawn delay",
          "Leave on 0 to disable this feature entirely - otherwise set number of days after which resources should respawn",
          options_range(0, 100, 1), 13),
      field("respawn_turfs", "Also respawn turfs", "If you enable resources respawn you can also allow turfs to respawn", options_bool, false),
      field("respawn_structure_exclusion_zone_size", "Base area", "Defines how far from structures should respawn be prevented", options_range(0,100,1),4),
    field("prefab_regen_delay_override", "Custom respawn delays",
          "You can specify custom prefab delays overrides in format prefab1:delay1,prefab2:delay2",
    {option("standard", "")},""),
    field("ranks", "Ranks", "Defines ranks used by command system",
      option(default_ranks, "Default"),default_ranks)
}
