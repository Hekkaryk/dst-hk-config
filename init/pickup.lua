local function log_pickup(act, note)
    if act == nil then
        log_d("Can't log nil action")
        return
    end
    if act.doer == nil then
        log_d("Can't log action done by nobody")
        return
    end

    local message

    if act.doer.name and act.doer.userid then
        message = "(" .. act.doer.userid .. ") " .. act.doer.name
    elseif act.doer.prefab then
        message = act.doer.prefab
    else
        message = "Something"
    end

    message = message .. note

    if act.target then
        if act.target.prefab then
            message = message .. act.target.prefab
        end

        if act.target.hkdata == nil then
            act.target.hkdata = {}
        end

        if act.target.hkdata.guid == nil then
            act.target.hkdata.guid = get_long_random_string()
            message = message .. " ( new GUID: "
        else
            message = message .. " ( GUID: "
        end

        message = message .. act.target.hkdata.guid .. " )"
    else
        message = message .. "... something. Not even game knows what. Let's just keep at that, ok?"
    end

    log_l(message)
end

_G.ACTIONS.PICKUP.fn = function(act)
    if
        act.doer.components.inventory ~= nil and act.target ~= nil and act.target.components.inventoryitem ~= nil and
            (act.target.components.inventoryitem.canbepickedup or
                (act.target.components.inventoryitem.canbepickedupalive and not act.doer:HasTag("player"))) and
            not (act.target:IsInLimbo() or
                (act.target.components.burnable ~= nil and act.target.components.burnable:IsBurning()) or
                (act.target.components.projectile ~= nil and act.target.components.projectile:IsThrown()))
     then
        if
            act.doer.components.itemtyperestrictions ~= nil and
                not act.doer.components.itemtyperestrictions:IsAllowed(act.target)
         then
            return false, "restriction"
        end

        act.doer:PushEvent("onpickupitem", {item = act.target})

        if
            not act.target.components.inventoryitem.cangoincontainer and act.target.components.equippable and
                act.doer.components.inventory:GetEquippedItem(act.target.components.equippable.equipslot)
         then
            log_pickup(act, " picks up backpack ")

            local item = act.doer.components.inventory:GetEquippedItem(act.target.components.equippable.equipslot)
            if item.components.inventoryitem and item.components.inventoryitem.cangoincontainer then
                act.doer.components.inventory:GiveItem(
                    act.doer.components.inventory:Unequip(act.target.components.equippable.equipslot)
                )
            else
                act.doer.components.inventory:DropItem(
                    act.doer.components.inventory:GetEquippedItem(act.target.components.equippable.equipslot)
                )
            end
            act.doer.components.inventory:Equip(act.target)
            return true
        end

        log_pickup(act, " picks up ")

        if
            act.doer:HasTag("player") and
                (act.target.components.equippable ~= nil and
                    not act.doer.components.inventory:GetEquippedItem(act.target.components.equippable.equipslot) or
                    act.doer.components.inventory:GetNumSlots() <= 0)
         then
            act.doer.components.inventory:Equip(act.target)
        else
            act.doer.components.inventory:GiveItem(act.target, nil, act.target:GetPosition())
        end
        return true
    end
end
