local CHARACTER_INGREDIENT = _G.CHARACTER_INGREDIENT

if config.cheap_telltale then
    local ingredients = {Ingredient("cutgrass", 3), Ingredient("spidergland", 1)}
    local telltaleheart = _G.AllRecipes["reviver"]
    telltaleheart.ingredients = {}
    telltaleheart.character_ingredients = {}
    for _, v in pairs(ingredients) do
        if table.contains(CHARACTER_INGREDIENT, v.type) then
            table.insert(telltaleheart.character_ingredients, v)
        else
            table.insert(telltaleheart.ingredients, v)
        end
    end
end
