-- disable blueprints drop from all containers
local blueprint = require "prefabs/blueprint"

blueprint.MakeAnyBlueprint =
    function()
    local inst = blueprint.fn()

    if not _G.TheWorld.ismastersim then
        return inst
    end

    local unknownrecipes = {}
    local knownrecipes = {}
    local allplayers = _G.AllPlayers
    for k, v in pairs(_G.AllRecipes) do
        if _G.IsRecipeValid(v.name) and blueprint.CanBlueprintRandomRecipe(v) then
            local known = false
            for i, player in ipairs(allplayers) do
                if player.components.builder:KnowsRecipe(v.name) or not player.components.builder:CanLearn(v.name) then
                    known = true
                    break
                end
            end
            table.insert(known and knownrecipes or unknownrecipes, v)
        end
    end
    inst.recipetouse =
        (#unknownrecipes > 0 and unknownrecipes[math.random(#unknownrecipes)].name) or
        (#knownrecipes > 0 and knownrecipes[math.random(#knownrecipes)].name) or
        "unknown"
    inst.components.teacher:SetRecipe(inst.recipetouse)
    if inst and inst.recipetouse and _G.STRINGS.NAMES[string.upper(inst.recipetouse)] and _G.STRINGS.NAMES.BLUEPRINT then
        inst.components.named:SetName(_G.STRINGS.NAMES[string.upper(inst.recipetouse)] .. " " .. _G.STRINGS.NAMES.BLUEPRINT)
        return inst
    end
end
