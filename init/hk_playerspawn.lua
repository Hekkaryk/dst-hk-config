local player_spawned = function(_, player)
    if config.enable_starting_items then
        local items = {}

        if config.custom_starting_items ~= "" then
            for x in first_level_string_split(config.custom_starting_items) do
                local name = ""
                local count = 0

                for y in second_level_string_split(x) do
                    if name == "" then
                        name = y
                    else
                        count = y
                    end
                end

                items[name] = count
            end
        end

        if config.enable_starting_items_seasonal then
            if GLOBAL.TheWorld.state.iswinter or
                (GLOBAL.TheWorld.state.isautumn and GLOBAL.TheWorld.state.remainingdaysinseason < 5) then

                items.cutgrass = items.cutgrass and items.cutgrass + 7 or 7
                items.twigs = items.twigs and items.twigs + 5 or 5
                items.log = items.log and items.log + 8 or 8
                items.meat = items.meat and items.meat + 4 or 4
                items.heatrock = items.heatrock and items.heatrock + 1 or 1
                items.winterhat = items.winterhat and items.winterhat + 1 or 1
            end

            -- During summer
            if GLOBAL.TheWorld.state.issummer or
                (GLOBAL.TheWorld.state.isspring and GLOBAL.TheWorld.state.remainingdaysinseason < 5) then

                items.cutgrass = items.cutgrass and items.cutgrass + 7 or 7
                items.nitre = items.nitre and items.nitre + 8 or 8
                items.ice = items.ice and items.ice + 15 or 15
                items.heatrock = items.heatrock and items.heatrock + 1 or 1
                items.strawhat = items.strawhat and items.strawhat + 1 or 1
            end

            -- During spring
            if GLOBAL.TheWorld.state.isspring or
                (GLOBAL.TheWorld.state.iswinter and GLOBAL.TheWorld.state.remainingdaysinseason < 3) then
                items.umbrella = items.umbrella and items.umbrella + 1 or 1
            end
        end

        if config.enable_starting_items_night and GLOBAL.TheWorld.state.isnight or
            (GLOBAL.TheWorld.state.isdusk and GLOBAL.TheWorld.state.timeinphase > .8) then
            items.torch = items.torch and items.torch + 1 or 1
        end

        if config.enable_starting_items_cave and GLOBAL.TheWorld:HasTag("cave") then
            items.minerhat = items.minerhat and items.minerhat + 1 or 1
        end

        if config.enable_starting_items_pvp and GLOBAL.TheNet:GetPVPEnabled() then
            items.spear = items.spear and items.spear + 1 or 1
            items.footballhat = items.footballhat and items.footballhat + 1 or 1
        end

        player.CurrentOnNewSpawn = player.OnNewSpawn or function()
            return true
        end

        player.OnNewSpawn = function(...)
            player.components.inventory.ignoresound = true

            for k, v in pairs(items) do
                for _ = 1, v, 1 do
                    local item = GLOBAL.SpawnPrefab(k)

                    if item == nil then
                        log_w("Skipping giving player " .. v .. " of " .. k .. ": prefab failed to spawn!")
                        break
                    end

                    player.components.inventory:GiveItem(item)
                end
            end

            player.components.inventory.ignoresound = true

            return player.CurrentOnNewSpawn(...)
        end
    end

    -- unfortunately, GLOBAL.TheWorld.minimap.MiniMap:ShowArea does not work anymore - until I find way to make this work...
    -- if config.reveal_map then
    --     player:DoTaskInTime(5, function(player)
    --         if config.reveal_map ~= "no" then
    --             local width, height = GLOBAL.TheWorld.Map:GetSize()

    --             for x = -2 * width + 20, 2 * width - 20, 2 do
    --                 for y = -2 * height + 20, 2 * height - 20, 2 do
    --                     if config.reveal_map == "roads" and GLOBAL.RoadManager:IsOnRoad(x, 0, y) then
    --                         GLOBAL.TheWorld.minimap.MiniMap:ShowArea(x, 0, y, 15)
    --                     elseif config.reveal_map == "all" and GLOBAL.TheWorld.Map:IsSurroundedByWater(x, 0, y, 4) ~=
    --                         false then
    --                         GLOBAL.TheWorld.minimap.MiniMap:ShowArea(x, 0, y, 15)
    --                     end
    --                 end
    --             end
    --         end
    --     end)
    -- end
end

AddPrefabPostInit("world", function(inst)
    inst:ListenForEvent("ms_playerspawn", player_spawned, inst)
end)
