if config["resistant_structures"] > -1 then
    -- local bearger = require "prefabs/bearger"
    function new_WorkEntities(inst)
        local x, y, z = inst.Transform:GetWorldPosition()
        local heading_angle = inst.Transform:GetRotation() * _G.DEGREES
        local x1, z1 = math.cos(heading_angle), -math.sin(heading_angle)
        for i, v in ipairs(_G.TheSim:FindEntities(x, 0, z, 5, nil, {
            "insect",
            "INLIMBO"
        }, {
            "CHOP_workable",
            "DIG_workable",
            "HAMMER_workable",
            "MINE_workable"
        })) do
            local x2, y2, z2 = v.Transform:GetWorldPosition()
            local dx, dz = x2 - x, z2 - z
            local len = math.sqrt(dx * dx + dz * dz)
            -- Normalized, then Dot product
            if v.components.workable and (len <= 0 or x1 * dx / len + z1 * dz / len > .3) then -- this fixes the crash when bearger tries to wreck things without workable component
                v.components.workable:Destroy(inst)
            end
        end
    end

    AddPrefabPostInit("bearger", function(inst)
        inst.WorkEntities = new_WorkEntities
    end)

    local function make_resistant(inst)
        inst.components.workable = nil
    end
    local resistant_structures = {
        "dragonflychest",
        "ancient_altar"
    }
    if config["resistant_structures"] > 1 then
        table.insert(resistant_structures, "icebox")
    end

    for _, v in pairs(resistant_structures) do
        AddPrefabPostInit(v, make_resistant)
        log_d(v .. " is no longer workable")
    end
end

if config["chests_protection_selection"] and config["chests_protection_selection"] > 0 then
    -- from: dragonfly_chest.lua
    local function new_onhammered(inst, worker)
        if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
            inst.components.burnable:Extinguish()
        end
        inst.components.lootdropper:DropLoot()
        if inst.components.container ~= nil then
            -- inst.components.container:DropEverything()
        end
        local fx = _G.SpawnPrefab("collapse_small")
        fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
        if inst.prefab == "icebox" then
            fx:SetMaterial("metal")
        else
            fx:SetMaterial("wood")
        end
        inst:Remove()
        local prettyname = ""
        if inst.prefab == "treasurechest" then
            prettyname = "a Chest"
        elseif inst.prefab == "dragonflychest" then
            prettyname = "a Scaled Chest"
        elseif inst.prefab == "icebox" then
            prettyname = "an Ice Box"
        else
            prettyname = inst.prefab
        end
        announce(worker.name .. " hammered down " .. prettyname .. "!")
        log_l(worker.name .. " destroyed " .. prettyname .. "!")
        -- .. " with guid == " .. inst.guid)
    end

    local function new_onhit(inst, worker)
        if not inst:HasTag("burnt") then
            inst.AnimState:PlayAnimation("hit")
            inst.AnimState:PushAnimation("closed", false)
            if inst.components.container ~= nil then
                -- inst.components.container:DropEverything()
                inst.components.container:Close()
            end
        end
        log_l(worker.name .. " hits a " .. inst.prefab)
        -- .. " with guid == " .. inst.guid)
    end

    local function modify_chests_workability(inst)
        if inst.components.workable then
            inst.components.workable:SetOnFinishCallback(new_onhammered)
            inst.components.workable:SetOnWorkCallback(new_onhit)
            if config["chests_protection_durability"] and config["chests_protection_durability"] > 0 then
                inst.components.workable:SetWorkLeft(config["chests_protection_durability"])
                log_d(inst.prefab .. " SetWorkLeft(" .. config["chests_protection_durability"] .. ")")
            end
        end
    end

    local selected_chests = {
        "dragonflychest"
    }
    if config["chests_protection_selection"] > 1 then
        table.insert(selected_chests, "icebox")
        table.insert(selected_chests, "treasurechest")
    end

    for _, v in pairs(selected_chests) do
        AddPrefabPostInit(v, modify_chests_workability)
        log_l(v .. " will no longer drop items when hit/hammered")
    end

    -- prevent item drop from burned down chests
    if config["chests_protection_strategy"] > 1 then

        -- this one needs to be regulary updated!
        _G.DefaultBurntStructureFn = function(inst)
            inst:AddTag("burnt")
            inst.components.burnable.canlight = false
            if inst.AnimState then
                inst.AnimState:PlayAnimation("burnt", true)
            end
            inst:PushEvent("burntup")
            if inst.SoundEmitter then
                inst.SoundEmitter:KillSound("idlesound")
                inst.SoundEmitter:KillSound("sound")
                inst.SoundEmitter:KillSound("loop")
                inst.SoundEmitter:KillSound("snd")
            end
            if inst.MiniMapEntity then
                inst.MiniMapEntity:SetEnabled(false)
            end
            if inst.components.workable then
                inst.components.workable:SetWorkLeft(1)
            end
            if inst.components.childspawner then
                if inst:GetTimeAlive() > 5 then
                    inst.components.childspawner:ReleaseAllChildren()
                end
                inst.components.childspawner:StopSpawning()
                inst:RemoveComponent("childspawner")
            end
            if inst.components.container then
                -- inst.components.container:DropEverything()
                inst.components.container:Close()
                inst:RemoveComponent("container")
            end
            if inst.components.dryer then
                inst.components.dryer:StopDrying("fire")
                inst:RemoveComponent("dryer")
            end
            if inst.components.stewer then
                inst.components.stewer:StopCooking("fire")
                inst:RemoveComponent("stewer")
            end
            if inst.components.harvestable then
                inst.components.harvestable:StopGrowing()
                inst:RemoveComponent("harvestable")
            end
            if inst.components.sleepingbag then
                inst:RemoveComponent("sleepingbag")
            end
            if inst.components.grower then
                inst.components.grower:Reset("fire")
                inst:RemoveComponent("grower")
            end
            if inst.components.spawner ~= nil then
                if inst:GetTimeAlive() > 5 and inst.components.spawner:IsOccupied() then
                    inst.components.spawner:ReleaseChild()
                end
                inst:RemoveComponent("spawner")
            end
            if inst.components.prototyper ~= nil then
                inst:RemoveComponent("prototyper")
            end
            if inst.components.wardrobe ~= nil then
                inst:RemoveComponent("wardrobe")
            end
            if inst.Light then
                inst.Light:Enable(false)
            end
            if inst.components.burnable then
                inst:RemoveComponent("burnable")
            end
        end
    end
end

if config.no_destroying_days > 0 then
    AddComponentPostInit("workable", function(workable, inst)
        if GLOBAL.TheWorld.ismastersim then
            workable.hk_previous_WorkedBy = workable.WorkedBy or function()
                return true
            end

            function workable:WorkedBy(worker, numworks)
                local age = (worker and worker.components and worker.components.age) and
                                worker.components.age:GetAgeInDays() or 0

                if not inst:HasTag("structure") or non_player_structures[workable.inst.prefab] or age >=
                    config.no_destroying_days then
                    return workable:hk_previous_WorkedBy(worker, numworks)
                elseif worker.components.talker then
                    worker.components.talker:Say("I cannot break this (" .. config.no_destroying_days - age ..
                                                     " more days).")
                end
            end
        end
    end)
end
