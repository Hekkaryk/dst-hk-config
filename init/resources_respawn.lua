-- for later: spliting string by space
-- local parts = {}
-- for part in string.gmatch(fixed_line, "%S+") do
-- table.insert(parts, part)
-- end
local ok, error = GLOBAL.pcall(function()

    local function respawn_init(inst)
        if GLOBAL.TheWorld.ismastersim then
            inst:AddComponent("hk_respawnable_controller")
            inst.components.hk_respawnable_controller:Configure(math.ceil(config.resources_respawn_delay), config.respawn_turfs, config.respawn_structure_exclusion_zone_size)
        end
    end

    AddPrefabPostInit("forest", respawn_init)
    AddPrefabPostInit("cave", respawn_init)
    local respawn_rates = {1, {"fireflies"},
                           0.5 * config.resources_respawn_delay, {"berrybush", "grass", "rock1", "rock2", "sapling"},
                           config.resources_respawn_delay,
                            {"beefalo", "beehive", "bishop", "cactus", "catcoonden", "cave_banana_tree", "cave_fern",
                             "chessjunk1", "chessjunk2", "chessjunk3", "flower_evil", "knight", "lichen", "livingtree",
                             "mandrake_planted", "marblepillar", "marbletree", "marsh_bush", "mermhouse", "minotaur",
                             "mound", "red_mushroom", "green_mushroom", "blue_mushroom", "pighouse", "rabbithouse",
                             "reeds", "rock_tough", "rock_flintless", "slurtlehole", "rook", "ruins_table",
                             "ruins_chair", "ruins_vase", "ruins_plate", "ruins_bowl", "ruins_chipbowl", "stalagmite",
                             "stalagmite_tall", "statueharp", "statuemaxwell", "statueruins", "pighead", "mermhead",
                             "tentacle", "ancient_altar", "ancient_altar_broken", "ruins_statue_mage",
                             "ruins_statue_mage_nogem", "houndbone", "stalagmite", "rocky", "bat", "worm",
                             "bishop_nightmare", "rook_nightmare", "knight_nightmare", "resurrectionstone",
                             "pandoraschest", "houndmound", "molehill", "pigtorch", "spiderden", "spiderhole",
                             "tallbirdnest", "wasphive", "appletree", "berryblue", "berryblu2", "berrygree",
                             "berrygre2", "pineapple", "beet_planted", "onion_planted", "radish_planted",
                             "potato_planted", "livingjungletree", "bambootree", "cottontree", "beardlordhouse",
                             "cherry_tree", "tuber_tree", "rainforesttree", "lightninggoat", "mangoling_den",
                             "crabhole", "mushtree", "perma_grassgekko", "perma_mosquito", "perma_pond_mos",
                             "petrified_tree_metal", "mushtree_metal", "pighouse_blue", "pig_cyborg", "pighouse_fire",
                             "pighouse_gray", "pighouse_yellow", "poisonhole", "primeapebarrel", "q_spiceshrub",
                             "q_sugarwoodtree", "rock_iron", "rock_obsidian", "rock_charcoal", "sandhill",
                             "seaweed_planted", "seashell_beached", "slurtlehole", "wildbeaver_house", "wildbore_house",
                             "wildmeanver_house", "yeti", "doydoy", "lobsterhole", "mutant_rabbithouse", "clawpalmtree",
                             "desertpalm", "ballphinhouse", "lilybush", "rosebush", "orchidbush", "bittersweetbush",
                             "bunneefalo", "brown_mushroom", "bush_vine", "coffeebush", "frosty_lightberrybush",
                             "gray_mushroom", "hydrid_banana_tree", "jungletree", "jungletrees",
                             "reingoat", "mango_planted", "mermhouse_blue", "mintybush", "palmtree", "palmtrees",
                             "peach_tree", "pufftree", "tea_plant", "yellow_mushroom", "quagmire_sugarwoodtree",
                             "quagmire_spotspice_shrub", "mermfishhouse", "mango_tree", "berrybush2_snake",
                             "des_chai_tree", "hmato_planted", "des_ivan_chai_tree"},
                           2 * config.resources_respawn_delay - 1, {"berrybush_juicy", "twiggytree"}}

    local respawn_delay_overrides = {}

    for _, pair in first_level_string_split(config.prefab_regen_delay_override) do
        local name_delay = second_level_string_split(pair)

        if name_delay ~= nil and name_delay[1] ~= nil and name_delay[1] ~= "" and name_delay[2] ~= nil and name_delay[2] ~=
            "" and GLOBAL.tonumber(name_delay[2]) ~= nil then
            respawn_delay_overrides[name_delay[1]] = math.clamp(math.ceil(GLOBAL.tonumber(name_delay[2])), 0,
                                                         config.resources_respawn_delay)
        end
    end

    log_d("#respawn_rates ==" .. #respawn_rates)

    local mode = false
    local default_prefab_delay = nil

    for _, data in ipairs(respawn_rates) do

        if mode == false then
            default_prefab_delay = data
        else
            log_d("delay == " .. default_prefab_delay .. " #prefab_list == " .. #data)

            for _, prefab in ipairs(data) do
                if respawn_delay_overrides[prefab] == nil then
                    AddPrefabPostInit(prefab, function(inst)
                        if GLOBAL.TheWorld.ismastersim then
                            inst:AddComponent("hk_respawnable")
                            inst.components.hk_respawnable.delay = default_prefab_delay
                        end
                    end)

                    log_i("hk_respawnable added to all " .. prefab .. " with delay " .. default_prefab_delay)
                end
            end
        end

        mode = not mode
    end

    for prefab, delay in ipairs(respawn_delay_overrides) do
        AddPrefabPostInit(prefab, function(inst)
            if GLOBAL.TheWorld.ismastersim then
                inst:AddComponent("hk_respawnable")
                inst.components.hk_respawnable.delay = delay
            end
        end)

        log_i("hk_respawnable added to all " .. prefab .. " with delay " .. delay .. " as part of requested override")
    end

    local currentActionDigFn = GLOBAL.ACTIONS.DIG.fn or function()
        return true
    end
    GLOBAL.ACTIONS.DIG.fn = function(act) -- Mounds do not disappear, and thus are not marked for respawning normally...
        if act.target and act.target.prefab == "mound" then
            act.target:DoTaskInTime(TUNING.SEG_TIME,
                function(inst) -- Saving and loading server will cause this task to cancel, thus we don't want a too long time to ensure it will regenerate
                    inst:Remove()
                end)
        end
        return currentActionDigFn(act)
    end

    if config.respawn_turfs and GLOBAL.TheNet:GetIsServer() then
        AddComponentPostInit("terraformer", function(self, inst)
            if not GLOBAL.TheWorld.ismastersim then -- redundant due to TheNet:GetIsServer check above, just to be safe
                return
            end

            local prev_Terraform = self.Terraform

            self.Terraform = function(self, pt, ...)
                local ret = prev_Terraform(self, pt, ...)
                local world = GLOBAL.TheWorld

                if ret and world and world.components.hk_respawnable_controller then
                    local x, y = world.Map:GetTileCoordsAtPoint(pt:Get())
                    world.components.hk_respawnable_controller:AddTurfRespawn(x, y)
                end

                return ret
            end
        end)
    end

    log_i("Resource respawn setup completed.")
end)

if not ok then log_d("resource_respawn failed to initialize: " .. error) end