AddComponentPostInit("lighter", function(lighter, inst)
    lighter.hk_previous_Light = lighter.Light or function()
        return true
    end

    function lighter:Light(target)
        if inst and inst.components and inst.components.inventoryitem and inst.components.inventoryitem.owner and
            inst.components.inventoryitem.owner.components and inst.components.inventoryitem.owner.components.age then
            local age = inst.components.inventoryitem.owner.components.age:GetAgeInDays()

            if age >= config.no_burning_days and
                (not target:HasTag("structure") or non_player_structures[target.prefab] or
                    (not_nil_or_empty(config.no_destroying_days) and config.no_destroying_days > 0 and age >=
                        config.no_destroying_days)) then
                return lighter:hk_previous_Light(target)
            elseif inst.components.inventoryitem.owner.components.talker then
                inst.components.inventoryitem.owner.components.talker:Say(
                    "I can't set it on fire (" .. config.no_burning_days - age .. " more days).")
            end
        end
    end
end)

for _, v in pairs({
    "ligher",
    "torch"
}) do
    AddPrefabPostInit(v, function(inst)
        if GLOBAL.TheWorld.ismastersim then
            inst.hk_previous_onattack = inst.onattack or function()
                return true
            end

            inst.onattack = function(weapon, attacker, target)
                if attacker and attacker.components and attacker.components.age then
                    local age = attacker.componants.age:GetAgeInDays()

                    if age >= config.no_burning_days and
                        (not target:HasTag("structure") or non_player_structures[target.prefab] or
                            (not_nil_or_empty(config.no_destroying_days) and config.no_destroying_days > 0 and age >= config.no_destroying_days)) then
                        return inst.hk_previous_onattack(weapon, attacker, target)
                    end
                end
            end
        end
    end)
end

AddComponentPostInit("firebug", function(inst)
    inst.hk_previous_OnUpdate = inst.OnUpdate

    inst.OnUpdate = function(inst, dt)
        if inst and inst.components and inst.components.age and inst.components.age:GetAgeInDays() >= config.no_burning_days then
            return inst:hk_previous_OnUpdate(dt)
        end
    end
end)