local function RevealMap(inst, player)
    log_i("ping0.9")
    player:DoTaskInTime(5, function(player)
        log_i("ping1")
        if config.reveal_map ~= "no" then
            local width, height = GLOBAL.TheWorld.Map:GetSize()

            if width ~= nil then
                log_i("width" .. width)
            end
            if height ~= nil then
                log_i("height" .. height)
            end

            for i = -2 * width + 20, 2 * width - 20, 2 do
                for j = -2 * height + 20, 2 * height - 20, 2 do
                    if config.reveal_map == "roads" then
                        if GLOBAL.RoadManager:IsOnRoad(i, 0, j) then
                            log_i("ping3!")
                            GLOBAL.TheWorld.minimap.MiniMap:ShowArea(i, 0, j, 15)
                        end
                    elseif config.reveal_map == "all" then
                        if GLOBAL.TheWorld.Map:IsSurroundedByWater(i, 0, j, 4) ~= false then
                            log_i("ping4!")
                            GLOBAL.TheWorld.minimap.MiniMap:ShowArea(i, 0, j, 15)
                        end
                    end
                end
            end
            log_i("ping5")
        end
    end)
end

AddPrefabPostInit("world", function(inst)
    log_i("ping0.5")
    inst:ListenForEvent("playeractivated", RevealMap, inst)
end)