-- icebox perish rate
if config.food_perish_rate ~= 0.5 then
    _TUNING.PERISH_FRIDGE_MULT = config.food_perish_rate
    _TUNING.PERISH_SALTBOX_MULT = config.food_perish_rate
end

-- respawn penalty
if config.no_respawn_penalty then
    _TUNING.PORTAL_HEALTH_PENALTY = 0
    _TUNING.HEART_HEALTH_PENALTY = 0
    _TUNING.EFFIGY_HEALTH_PENALTY = 0
    _TUNING.REVIVE_HEALTH_PENALTY_AS_MULTIPLE_OF_EFFIGY = 0
    _TUNING.REVIVE_HEALTH_PENALTY = 0

    if config.command_revive_penalty and config.command_revive_penalty == 0 then
        _TUNING.MAXIMUM_HEALTH_PENALTY = 0
        _TUNING.MAXIMUM_SANITY_PENALTY = 0
    end
end

-- free hammering
if config.free_hammering then
    _TUNING.HAMMER_LOOT_PERCENT = 1
    _TUNING.BURNT_HAMMER_LOOT_PERCENT = 1
end

-- no (new) grass geckos
if config.no_grass_geckos then
    _TUNING.GRASSGEKKO_MORPH_CHANCE = 0
end

-- regrow rate
if config.regrow_rate ~= 1 then
    _TUNING.GRASS_REGROW_TIME = _TUNING.DAY_SEGS_DEFAULT * 3 * config.regrow_rate
    _TUNING.SAPLING_REGROW_TIME = _TUNING.DAY_SEGS_DEFAULT * 4 * config.regrow_rate
    _TUNING.MARSHBUSH_REGROW_TIME = _TUNING.DAY_SEGS_DEFAULT * 4 * config.regrow_rate
    _TUNING.CACTUS_REGROW_TIME = _TUNING.DAY_SEGS_DEFAULT * 4 * config.regrow_rate
    _TUNING.FLOWER_CAVE_REGROW_TIME = _TUNING.DAY_SEGS_DEFAULT * 3 * config.regrow_rate
    _TUNING.FLOWER_CAVE_RECHARGE_TIME = 110 * config.regrow_rate
    _TUNING.LICHEN_REGROW_TIME = _TUNING.DAY_SEGS_DEFAULT * 5 * config.regrow_rate
    _TUNING.BERRY_REGROW_TIME = _TUNING.DAY_SEGS_DEFAULT * 3 * config.regrow_rate
    _TUNING.BERRY_REGROW_INCREASE = _TUNING.DAY_SEGS_DEFAULT * .5 * config.regrow_rate
    _TUNING.BERRY_REGROW_VARIANCE = _TUNING.DAY_SEGS_DEFAULT * 2 * config.regrow_rate
    _TUNING.BERRY_JUICY_REGROW_TIME = _TUNING.DAY_SEGS_DEFAULT * 9 * config.regrow_rate
    _TUNING.BERRY_JUICY_REGROW_INCREASE = _TUNING.DAY_SEGS_DEFAULT * .5 * config.regrow_rate
    _TUNING.BERRY_JUICY_REGROW_VARIANCE = _TUNING.DAY_SEGS_DEFAULT * 2 * config.regrow_rate
    _TUNING.REEDS_REGROW_TIME = _TUNING.DAY_SEGS_DEFAULT * 3 * config.regrow_rate
end

-- berries cycles
if config.berries_cycles ~= 3 then
    _TUNING.BERRYBUSH_CYCLES = config.berries_cycles
    _TUNING.BERRYBUSH_JUICY_CYCLES = config.berries_cycles
end

-- stronger eyeturrets
if config.stronger_eyeturrets ~= 0 then
    if config.stronger_eyeturrets == 1 then
        _TUNING.EYETURRET_DAMAGE = 150
        _TUNING.EYETURRET_HEALTH = 2500
        _TUNING.EYETURRET_REGEN = 50
        _TUNING.EYETURRET_ATTACK_PERIOD = 2.5
    elseif config.stronger_eyeturrets == 2 then
        _TUNING.EYETURRET_DAMAGE = 300
        _TUNING.EYETURRET_HEALTH = 5000
        _TUNING.EYETURRET_REGEN = 100
        _TUNING.EYETURRET_ATTACK_PERIOD = 2
    elseif config.stronger_eyeturrets == 3 then
        _TUNING.EYETURRET_DAMAGE = 500
        _TUNING.EYETURRET_HEALTH = 7500
        _TUNING.EYETURRET_REGEN = 250
        _TUNING.EYETURRET_ATTACK_PERIOD = 1
    end
end

-- no blight
if config.no_blight then
    _TUNING.DISEASE_SPREAD_RADIUS = 0
    _TUNING.DISEASE_CHANCE = 0
end

local wilson_health = 150
local multiplayer_armor_durability_modifier = 0.7

local function remove_component(prefab, component)
    AddPrefabPostInit(prefab, function(inst)
        inst:RemoveComponent(component)
    end)
end

local function remove_finiteuses_component(array_of_prefabs)
    for _, v in pairs(array_of_prefabs) do
        remove_component(v, "finiteuses")
    end
end

local function remove_fueled_component(array_of_prefabs)
    for _, v in pairs(array_of_prefabs) do
        remove_component(v, "fueled")
    end
end

-- tool uses
if config.ratio_weapons == 0 then
    local tools = {
        "axe",
        "hammer",
        "shovel",
        "pitchfork",
        "pickaxe",
        "bugnet",
        "fishingrod",
        "trap",
        "multitool_ax_pickaxe",
        "saddlehorn",
        "brush",
        "umbrella",
        "sewingkit",
        "horn"
    }
    remove_finiteuses_component(tools)
elseif config.ratio_weapons ~= 1 then
    _TUNING.AXE_USES = 100 * config.ratio_weapons
    _TUNING.HAMMER_USES = 75 * config.ratio_weapons
    _TUNING.SHOVEL_USES = 25 * config.ratio_weapons
    _TUNING.PITCHFORK_USES = 25 * config.ratio_weapons
    _TUNING.PICKAXE_USES = 33 * config.ratio_weapons
    _TUNING.BUGNET_USES = 10 * config.ratio_weapons
    _TUNING.FISHINGROD_USES = 9 * config.ratio_weapons
    _TUNING.TRAP_USES = 8 * config.ratio_weapons
    _TUNING.MULTITOOL_AXE_PICKAXE_USES = 400 * config.ratio_weapons
    _TUNING.SADDLEHORN_USES = 10 * config.ratio_weapons
    _TUNING.BRUSH_USES = 75 * config.ratio_weapons
    _TUNING.UMBRELLA_USES = 20 * config.ratio_weapons
    _TUNING.SEWINGKIT_USES = 5 * config.ratio_weapons
    _TUNING.HORN_USES = 10 * config.ratio_weapons
end
-- weapon uses
if config.ratio_weapons == 0 then
    local weapons = {
        "whip",
        "spear",
        "claw_glove",
        "wathgrithr_spear",
        "spike",
        "boomerang",
        "nightsword",
        "hambat",
        "batbat",
        "ruins_bat",
        "trap_teeth"
    }
    remove_finiteuses_component(weapons)
elseif config.ratio_weapons ~= 1 then
    _TUNING.WHIP_USES = 175 * config.ratio_weapons
    _TUNING.SPEAR_USES = 150 * config.ratio_weapons
    _TUNING.CLAW_GLOVE_USES = 200 * config.ratio_weapons
    _TUNING.WATHGRITHR_SPEAR_USES = 200 * config.ratio_weapons
    _TUNING.SPIKE_USES = 100 * config.ratio_weapons
    _TUNING.BOOMERANG_USES = 10 * config.ratio_weapons
    _TUNING.NIGHTSWORD_USES = 100 * config.ratio_weapons
    _TUNING.HAMBAT_USES = 100 * config.ratio_weapons
    _TUNING.BATBAT_USES = 75 * config.ratio_weapons
    _TUNING.RUINS_BAT_USES = 150 * config.ratio_weapons
    _TUNING.TRAP_TEETH_USES = 10 * config.ratio_weapons
end
-- staff uses
if config.ratio_staffs == 0 then
    local staffs = {
        "icestaff",
        "firestaff",
        "yellowstaff",
        "telestaff",
        "opalstaff",
        "orangestaff",
        "greenstaff"
    }
    remove_finiteuses_component(staffs)
elseif config.ratio_staffs ~= 1 then
    _TUNING.ICESTAFF_USES = 20 * config.ratio_staffs
    _TUNING.FIRESTAFF_USES = 20 * config.ratio_staffs
    _TUNING.YELLOWSTAFF_USES = 20 * config.ratio_staffs
    _TUNING.TELESTAFF_USES = 5 * config.ratio_staffs
    _TUNING.OPALSTAFF_USES = 50 * config.ratio_staffs
    _TUNING.ORANGESTAFF_USES = 20 * config.ratio_staffs
    _TUNING.GREENSTAFF_USES = 5 * config.ratio_staffs
end
-- amulet uses
if config.ratio_amulets == 0 then
    local amulets_with_uses = {
        "redamulet",
        "orangeamulet",
        "greenamulet"
    }
    remove_finiteuses_component(amulets_with_uses)
    local amulets_with_fuel = {
        "blueamulet",
        "purpleamulet",
        "yellowamulet"
    }
    remove_fueled_component(amulets_with_fuel)
elseif config.ratio_amulets ~= 1 then
    _TUNING.REDAMULET_USES = 20 * config.ratio_amulets
    _TUNING.BLUEAMULET_FUEL = _TUNING.DAY_SEGS_DEFAULT * 0.75 * config.ratio_amulets
    _TUNING.PURPLEAMULET_FUEL = _TUNING.DAY_SEGS_DEFAULT * 0.4 * config.ratio_amulets
    _TUNING.YELLOWAMULET_FUEL = _TUNING.DAY_SEGS_DEFAULT * config.ratio_amulets
    _TUNING.ORANGEAMULET_USES = 225 * config.ratio_amulets
    _TUNING.GREENAMULET_USES = 5 * config.ratio_amulets
end
-- clothes durability
local ratio_clothes = config.ratio_clothes
if ratio_clothes == 0 then
    ratio_clothes = math.huge
end
if ratio_clothes ~= 1 then
    _TUNING.SPIDERHAT_PERISHTIME = 4 * _TUNING.SEG_TIME * ratio_clothes
    _TUNING.ONEMANBAND_PERISHTIME = 6 * _TUNING.SEG_TIME * ratio_clothes
    _TUNING.GRASS_UMBRELLA_PERISHTIME = 2 * _TUNING.DAY_SEGS_DEFAULT * ratio_clothes
    _TUNING.UMBRELLA_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 6 * ratio_clothes
    _TUNING.EYEBRELLA_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 9 * ratio_clothes
    _TUNING.STRAWHAT_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 5 * ratio_clothes
    _TUNING.EARMUFF_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 5 * ratio_clothes
    _TUNING.WINTERHAT_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 10 * ratio_clothes
    _TUNING.BEEFALOHAT_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 10 * ratio_clothes
    _TUNING.TRUNKVEST_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 15 * ratio_clothes
    _TUNING.REFLECTIVEVEST_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 8 * ratio_clothes
    _TUNING.HAWAIIANSHIRT_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 15 * ratio_clothes
    _TUNING.SWEATERVEST_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 10 * ratio_clothes
    _TUNING.HUNGERBELT_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 8 * ratio_clothes
    _TUNING.BEARGERVEST_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 7 * ratio_clothes
    _TUNING.RAINCOAT_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 10 * ratio_clothes
    _TUNING.WALRUSHAT_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 25 * ratio_clothes
    _TUNING.FEATHERHAT_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 8 * ratio_clothes
    _TUNING.TOPHAT_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 8 * ratio_clothes
    _TUNING.ICEHAT_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 4 * ratio_clothes
    _TUNING.MOLEHAT_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 1.5 * ratio_clothes
    _TUNING.RAINHAT_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 10 * ratio_clothes
    _TUNING.CATCOONHAT_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 10 * ratio_clothes
    _TUNING.GOGGLES_PERISHTIME = _TUNING.DAY_SEGS_DEFAULT * 10 * ratio_clothes
    _TUNING.SEWINGKIT_REPAIR_VALUE = _TUNING.DAY_SEGS_DEFAULT * 5 * ratio_clothes
    _TUNING.SEWING_TUNINGAPE_REPAIR_VALUE = _TUNING.DAY_SEGS_DEFAULT * 5 * ratio_clothes
end
-- armor uses
if config.ratio_armors == 0 then
    local function set_as_indestructible(inst)
        inst.indestructible = true
    end
    local armors = {
        "armorgrass",
        "armorwood",
        "armormarble",
        "armorsnurtleshell",
        "armorruins",
        "armorslurper",
        "armor_footballhat",
        "armordragonfly",
        "armor_wathgrithrhat",
        "armor_ruinshat",
        "armor_slurtlehat",
        "armor_beehat",
        "armor_sanity",
        "armor_hivehat",
        "armor_skeletonhat"
    }

    for _, v in pairs(armors) do
        AddPrefabPostInit(v, set_as_indestructible)
    end
elseif config.ratio_armors ~= 1 then
    _TUNING.ARMORGRASS = wilson_health * 1.5 * multiplayer_armor_durability_modifier * config.ratio_armors
    _TUNING.ARMORWOOD = wilson_health * 3 * multiplayer_armor_durability_modifier * config.ratio_armors
    _TUNING.ARMORMARBLE = wilson_health * 7 * multiplayer_armor_durability_modifier * config.ratio_armors
    _TUNING.ARMORSNURTLESHELL = wilson_health * 7 * multiplayer_armor_durability_modifier * config.ratio_armors
    _TUNING.ARMORRUINS = wilson_health * 12 * multiplayer_armor_durability_modifier * config.ratio_armors
    _TUNING.ARMORSLURPER = wilson_health * 4 * multiplayer_armor_durability_modifier * config.ratio_armors
    _TUNING.ARMOR_FOOTBALLHAT = wilson_health * 3 * multiplayer_armor_durability_modifier * config.ratio_armors
    _TUNING.ARMORDRAGONFLY = wilson_health * 9 * multiplayer_armor_durability_modifier * config.ratio_armors
    _TUNING.ARMOR_WATHGRITHRHAT = wilson_health * 5 * multiplayer_armor_durability_modifier * config.ratio_armors
    _TUNING.ARMOR_RUINSHAT = wilson_health * 8 * multiplayer_armor_durability_modifier * config.ratio_armors
    _TUNING.ARMOR_SLURTLEHAT = wilson_health * 5 * multiplayer_armor_durability_modifier * config.ratio_armors
    _TUNING.ARMOR_BEEHAT = wilson_health * 10 * multiplayer_armor_durability_modifier * config.ratio_armors
    _TUNING.ARMOR_SANITY = wilson_health * 5 * multiplayer_armor_durability_modifier * config.ratio_armors
    _TUNING.ARMOR_HIVEHAT = wilson_health * 9 * multiplayer_armor_durability_modifier * config.ratio_armors
    _TUNING.ARMOR_SKELETONHAT = wilson_health * 9 * multiplayer_armor_durability_modifier * config.ratio_armors
end
-- light duration
if config.ratio_light ~= 1 then
    _TUNING.TINY_FUEL = _TUNING.SEG_TIME * .25 * config.ratio_light
    _TUNING.SMALL_FUEL = _TUNING.SEG_TIME * .5 * config.ratio_light
    _TUNING.MED_FUEL = _TUNING.SEG_TIME * 1.5 * config.ratio_light
    _TUNING.MED_LARGE_FUEL = _TUNING.SEG_TIME * 3 * config.ratio_light
    _TUNING.LARGE_FUEL = _TUNING.SEG_TIME * 6 * config.ratio_light
    _TUNING.TINY_BURNTIME = _TUNING.SEG_TIME * .1 * config.ratio_light
    _TUNING.SMALL_BURNTIME = _TUNING.SEG_TIME * .25 * config.ratio_light
    _TUNING.MED_BURNTIME = _TUNING.SEG_TIME * 0.5 * config.ratio_light
    _TUNING.LARGE_BURNTIME = _TUNING.SEG_TIME * config.ratio_light
    _TUNING.CAMPFIRE_FUEL_MAX = (_TUNING.NIGHT_SEGS_DEFAULT + _TUNING.DUSK_SEGS_DEFAULT) * 1.5 * config.ratio_light
    _TUNING.CAMPFIRE_FUEL_START = (_TUNING.NIGHT_SEGS_DEFAULT + _TUNING.DUSK_SEGS_DEFAULT) * .75 * config.ratio_light
    _TUNING.COLDFIRE_FUEL_MAX = (_TUNING.NIGHT_SEGS_DEFAULT + _TUNING.DUSK_SEGS_DEFAULT) * 1.5 * config.ratio_light
    _TUNING.COLDFIRE_FUEL_START = (_TUNING.NIGHT_SEGS_DEFAULT + _TUNING.DUSK_SEGS_DEFAULT) * .75 * config.ratio_light
    _TUNING.ROCKLIGHT_FUEL_MAX = (_TUNING.NIGHT_SEGS_DEFAULT + _TUNING.DUSK_SEGS_DEFAULT) * 1.5 * config.ratio_light
    _TUNING.FIREPIT_FUEL_MAX = (_TUNING.NIGHT_SEGS_DEFAULT + _TUNING.DUSK_SEGS_DEFAULT) * 2 * config.ratio_light
    _TUNING.FIREPIT_FUEL_START = _TUNING.NIGHT_SEGS_DEFAULT + _TUNING.DUSK_SEGS_DEFAULT * config.ratio_light
    _TUNING.COLDFIREPIT_FUEL_MAX = (_TUNING.NIGHT_SEGS_DEFAULT + _TUNING.DUSK_SEGS_DEFAULT) * 2 * config.ratio_light
    _TUNING.COLDFIREPIT_FUEL_START = _TUNING.NIGHT_SEGS_DEFAULT + _TUNING.DUSK_SEGS_DEFAULT * config.ratio_light
    _TUNING.PIGTORCH_FUEL_MAX = _TUNING.NIGHT_SEGS_DEFAULT * config.ratio_light
    _TUNING.NIGHTLIGHT_FUEL_MAX = (_TUNING.NIGHT_SEGS_DEFAULT + _TUNING.DUSK_SEGS_DEFAULT) * 3 * config.ratio_light
    _TUNING.NIGHTLIGHT_FUEL_START = (_TUNING.NIGHT_SEGS_DEFAULT + _TUNING.DUSK_SEGS_DEFAULT) * config.ratio_light
    _TUNING.TORCH_FUEL = _TUNING.NIGHT_SEGS_DEFAULT * 1.25 * config.ratio_light
    _TUNING.MINIFAN_FUEL = _TUNING.DAY_SEGS_DEFAULT * 0.3 * config.ratio_light
    _TUNING.COMPASS_FUEL = _TUNING.DAY_SEGS_DEFAULT * 4 * config.ratio_light
    _TUNING.NIGHTSTICK_FUEL = _TUNING.NIGHT_SEGS_DEFAULT * 6 * config.ratio_light
    _TUNING.LIGHTER_FUEL = _TUNING.DAY_SEGS_DEFAULT * 1.25 * config.ratio_light
    _TUNING.MINERHAT_LIGHTTIME = (_TUNING.NIGHT_SEGS_DEFAULT + _TUNING.DUSK_SEGS_DEFAULT) * 2.6 * config.ratio_light
    _TUNING.LANTERN_LIGHTTIME = (_TUNING.NIGHT_SEGS_DEFAULT + _TUNING.DUSK_SEGS_DEFAULT) * 2.6 * config.ratio_light
end
-- boss hp ratio
if config.ratio_boss_hp ~= 1 then
    _TUNING.BEARGER_HEALTH = math.ceil(3000 * 2 * config.ratio_boss_hp)
    _TUNING.DRAGONFLY_HEALTH = math.ceil(27500 * config.ratio_boss_hp)
    _TUNING.BEEQUEEN_HEALTH = math.ceil(22500 * config.ratio_boss_hp)
    _TUNING.TOADSTOOL_HEALTH = math.ceil(52500 * config.ratio_boss_hp)
    _TUNING.SPIDERQUEEN_HEALTH = math.ceil(1250 * 2 * config.ratio_boss_hp)
    _TUNING.DEERCLOPS_HEALTH = math.ceil(2000 * 2 * config.ratio_boss_hp)
    _TUNING.LEIF_HEALTH = math.ceil(2000 * 1.5 * config.ratio_boss_hp)
    _TUNING.KLAUS_HEALTH = math.ceil(10000 * config.ratio_boss_hp)
    _TUNING.MINOTAUR_HEALTH = math.ceil(2500 * 4 * config.ratio_boss_hp)
    _TUNING.MOOSE_HEALTH = math.ceil(3000 * 2 * config.ratio_boss_hp)
    _TUNING.SHADOW_ROOK.HEALTH = {
        math.ceil(1000 * config.ratio_boss_hp),
        math.ceil(4000 * config.ratio_boss_hp),
        math.ceil(10000 * config.ratio_boss_hp)
    }
    _TUNING.SHADOW_KNIGHT.HEALTH = {
        math.ceil(900 * config.ratio_boss_hp),
        math.ceil(2700 * config.ratio_boss_hp),
        math.ceil(8100 * config.ratio_boss_hp)
    }
    _TUNING.SHADOW_BISHOP.HEALTH = {
        math.ceil(800 * config.ratio_boss_hp),
        math.ceil(2500 * config.ratio_boss_hp),
        math.ceil(7500 * config.ratio_boss_hp)
    }
    _TUNING.TOADSTOOL_DARK_HEALTH = math.ceil(99999 * config.ratio_boss_hp)
    _TUNING.ANTLION_HEALTH = math.ceil(6000 * config.ratio_boss_hp)
end

if not_nil_or_empty(config.custom_tuning) then
    for name_value in first_level_string_split(config.custom_tuning) do
        if name_value == nil or name_value == "" then
            log_w(
                "malformatted custom_tuning format - encountered more than one space, comma or semicolon in a row - ignoring and attempting to continue")
        else
            local name = nil
            local value = nil

            for part in second_level_string_split(name_value) do
                if name == nil then
                    name = part
                else
                    value = part
                end
            end

            if not_nil_or_empty(name) and not_nil_or_empty(value) then
                log_d("Setting " .. name .. " to " .. value)
                _TUNING[name] = value
            else
                log_w(
                    "malformatted entry in custom_tuning - name_value divided with colon or equal sign contained empty string: " ..
                        name_value .. " - ignoring and attempting to continue")
            end
        end

    end
end