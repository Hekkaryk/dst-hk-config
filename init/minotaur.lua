if config.minotaur_respawn then
    AddPrefabPostInit(
        "world",
        function(TheWorld)
            TheWorld:AddComponent("minotaurspawner")
            TheWorld:DoTaskInTime(
                0,
                function(TheWorld)
                    if not (TheWorld.topology and TheWorld.topology.nodes) then
                        return
                    end
                    local found = 0
                    --local nodes = "[Hk]Config++ World node list:#"
                    for id, node in pairs(TheWorld.topology.nodes) do
                        --nodes = nodes .. id .. TheWorld.topology.ids[id] .. "#"
                        if
                            string.match(TheWorld.topology.ids[id], "RuinedGuarden") or -- (intended?) typo in original source code, verified
                                string.match(TheWorld.topology.ids[id], "LabyrinthGuarden") or -- same
                                string.match(
                                    TheWorld.topology.ids[id],
                                    "InfusedWorld_MinotaurHome:0:InfusedWorld_MinotaurHome_Spawn"
                                )
                         then -- Megarandom worlds with caves above
                            TheWorld.components.minotaurspawner:RegisterLocation(id, node.x, node.y)
                            found = found + 1
                        end
                    end
                    if found == 0 then
                        --print(nodes)
                        log_e(
                            "Failed to find Ancient_Guardian spawn point or instance. Respawning won't work."
                        )
                    elseif found == 1 then
                        log_i("Registered one Ancient_Guardian spawn point.")
                    else
                        log_i("Registered " .. found .. " Ancient_Guardian spawn points.")
                    end
                end
            )
        end
    )
end
