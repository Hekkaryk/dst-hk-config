cleanup_filenames = {}
cleanup_filenames.limits = "hk_config_cleanup_limits.txt"
cleanup_filenames.ignoredtags = "hk_config_cleanup_ignoredtags.txt"
cleanup_filenames.birdfood = "hk_config_cleanup_birdfood.txt"
local removed_count = 0
local prefab_count = {}
local prefab_limit = {}
local prefab_ignoredtags = {}

for _, v in pairs(cleanup_filenames) do
    if not file_check(v) then
        if file_create(v) then
            log_l("File " .. v .. " creted")
        else
            log_e("Could not create file " .. v .. " - cleanup might fail to work properly")
        end
    end
end

local data = {}

local function reload_data()
    data.limits = io.open(cleanup_filenames.limits, "rb")
    if data.limits then
        prefab_limit = nil
        prefab_limit = {}
        for line in data.limits:lines() do
            --local fixed_line = line:gsub("\r", "")
            local parts = {}
            for part in string.gmatch(line, "%S+") do
                table.insert(parts, part)
            end
            if parts[1] and parts[2] then
                prefab_limit[parts[1]] = _G.tonumber(parts[2])
            else
                log_e("Corrupted entry found in file " .. cleanup_filenames.limits)
            end
        end
    else
        log_e("Failed to open limits data")
    end

    data.ignoredtags = io.open(cleanup_filenames.ignoredtags, "rb")
    if data.ignoredtags then
        prefab_ignoredtags = nil
        prefab_ignoredtags = {}
        for line in data.ignoredtags:lines() do
            --local fixed_line = line:gsub("\r", "")
            local parts = {}
            for part in string.gmatch(line, "%S+") do
                table.insert(parts, part)
            end
            if parts[1] and parts[2] then
                prefab_ignoredtags[parts[1]] = {}
                for i = 2, #parts, 1 do
                    table.insert(prefab_ignoredtags[parts[1]], parts[i])
                    log_i("Cleanup will ignore " .. parts[1] .. " with tag " .. parts[i])
                end
            else
                log_e("Corrupted entry found in file " .. cleanup_filenames.limits)
            end
        end
    else
        log_e("Failed to open limits data")
    end
end

reload_data()

local function have_excluded_tag(inst)
    for _, v in ipairs(prefab_ignoredtags[inst.prefab]) do
        if inst:HasTag(v) then
            log_d("Ignoring " .. inst.prefab .. " because it have ignored tag " .. v)
            return true
        else
            log_d(inst.prefab .. " has no tag " .. v)
        end
    end
    log_d(inst.prefab .. " was not ignored because it has none of excluded tags!")
    return false
end

local function is_domesticated(inst)
    if
        inst and inst.components and inst.components.domesticatable and
            (inst.components.domesticatable:IsDomesticated() or
                (inst.components.domesticatable.domestication and inst.components.domesticatable.domestication > 0))
     then
        log_d(inst.prefab .. " was ignored because it is domesticated")
        return true
    else
        log_d(inst.prefab .. " was not ignored because it is not domesticated")
        if inst.components.domesticatable and inst.components.domesticatable.domestication then
            log_d(inst.prefab .. " domestication is: " .. inst.components.domesticatable.domestication)
        else
            log_d(inst.prefab .. " lacks inst.components.domesticatable.domestication")
        end
        return false
    end
end

if config.cleanup_cooldown - config.cleanup_warning <= 1 then
    log_i("Cleanup warning time is invalid. Ignoring it and setting warning time to 30 seconds")
end
local warning_time = config.cleanup_cooldown - config.cleanup_warning > 1 and config.cleanup_warning or 30
if config.cleanup_cooldown - warning_time <= 15 then
    log_i("Cleanup cooldown time is invalid. Ignoring it and setting cooldown time to 300 seconds")
end
local cooldown_time = config.cleanup_cooldown - warning_time > 15 and config.cleanup_cooldown or 300

local function format_time(raw_s)
    if raw_s == 0 then
        return "now"
    end
    local hours = math.floor(raw_s / 3600)
    local minutes = math.floor(raw_s / 60 - hours * 60)
    local seconds = math.floor(raw_s - hours * 3600 - minutes * 60)

    local time_string = "in "
    if hours ~= 0 then
        time_string = hours .. " hour" .. s_if_plural(hours)
    end
    if hours ~= 0 and minutes ~= 0 then
        time_string = time_string .. " "
    end
    if minutes ~= 0 then
        time_string = time_string .. minutes .. " minute" .. s_if_plural(minutes)
    end
    if (minutes ~= 0 and seconds ~= 0) or (hours ~= 0 and minutes == 0 and seconds ~= 0) then
        time_string = time_string .. " "
    end
    if seconds ~= 0 then
        time_string = time_string .. seconds .. " second" .. s_if_plural(seconds)
    end
    return time_string
end

local function cleanup_execute()
    for k, _ in pairs(prefab_count) do
        prefab_count[k] = 0
    end

    for _, v in pairs(_G.Ents) do
        if
            prefab_limit[v.prefab] and not is_owned_inventoryitem(v) and
                not (prefab_ignoredtags[v.prefab] and have_excluded_tag(v)) and
                not (config.cleanup_ignore_domesticated and is_domesticated(v))
         then
            if prefab_count[v.prefab] == nil then
                prefab_count[v.prefab] = 0
            end
            if
                (prefab_limit[v.prefab] and prefab_limit[v.prefab] == 0) or
                    (prefab_count[v.prefab] > prefab_limit[v.prefab])
             then
                v:Remove()
                removed_count = removed_count + 1
            else
                prefab_count[v.prefab] = prefab_count[v.prefab] + 1
            end
        end
    end
end

function cleanup_warning()
    local level_name = is_cave() and "Caves" or "Surface"

    announce("" .. level_name .. " will be cleaned " .. format_time(warning_time) .. "!")
    log_l(level_name .. " will be cleaned " .. format_time(warning_time))
    if _G.TheWorld.hk_config_cleanup_task ~= nil then
        _G.TheWorld.hk_config_cleanup_task:Cancel()
        _G.TheWorld.hk_config_cleanup_task = nil
    end
    _G.TheWorld.hk_config_cleanup_task = _G.TheWorld:DoTaskInTime(warning_time, cleanup_start)
end

function cleanup_start()
    local level_name = is_cave() and "caves" or "surface"
    announce("Cleaning " .. level_name .. "...")
    log_l("Cleaning " .. level_name .. "...")
    removed_count = 0
    cleanup_execute()
    announce("Removed " .. removed_count .. " objects from " .. level_name)
    announce("Next cleanup " .. format_time(cooldown_time))
    if _G.TheWorld.hk_config_cleanup_task ~= nil then
        _G.TheWorld.hk_config_cleanup_task:Cancel()
        _G.TheWorld.hk_config_cleanup_task = nil
    end
    _G.TheWorld.hk_config_cleanup_task = _G.TheWorld:DoTaskInTime(cooldown_time - warning_time, cleanup_warning)
end

local function add_cleanup_to_world(world)
    if not ismastersim() then
        return
    end

    world.hk_config_cleanup_task = world:DoTaskInTime(cooldown_time - warning_time, cleanup_warning)
end

AddPrefabPostInit("forest", add_cleanup_to_world)
AddPrefabPostInit("cave", add_cleanup_to_world)

_G.c_hkclean = function(warn)
    if not warn then
        cleanup_start()
    else
        cleanup_warning()
    end
end

_G.c_hkclean_reload = function()
    reload_data()
end

_G.c_hkclean_pause = function()
    if _G.TheWorld.hk_config_cleanup_task ~= nil then
        _G.TheWorld.hk_config_cleanup_task:Cancel()
        _G.TheWorld.hk_config_cleanup_task = nil
    end
end