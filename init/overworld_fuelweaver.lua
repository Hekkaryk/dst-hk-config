-- this will fail if another mod will remove areaawaware component from player (and won't work anymore) - like in Megarandom overworld Nightmare Cycles
if IsServer then
    local ATRIUM_RANGE = 8.5

    local function ActiveStargate(gate)
        return gate:IsWaitingForStalker()
    end

    --    local function IsNearAtrium(inst, other)
    --      local stargate = inst.components.entitytracker:GetEntity("stargate")
    --      return stargate ~= nil and stargate:IsObjectInAtriumArena(other or inst) and stargate:HasTag("intense")
    --    end

    AddPrefabPostInit(
        "fossil_stalker",
        function(inst)
            local function ItemTradeTest(inst, item, giver)
                if item == nil or item.prefab ~= "shadowheart" or giver == nil or giver.components.areaaware == nil then
                    return false
                elseif inst.form ~= 1 then
                    return false, "WRONGSHADOWFORM"
                elseif
                    giver.components.areaaware:CurrentlyInTag("Atrium") and
                        (_G.FindEntity(inst, ATRIUM_RANGE, ActiveStargate, {"stargate"}) == nil or
                            _G.GetClosestInstWithTag("stalker", inst, 40) ~= nil)
                 then
                    return false, "CANTSHADOWREVIVE"
                end
                return true
            end

            local function OnAccept(inst, giver, item)
                if item.prefab == "shadowheart" then
                    local stalker
                    local stargate = _G.FindEntity(inst, ATRIUM_RANGE, ActiveStargate, {"stargate"})
                    if stargate ~= nil then
                        stalker = _G.SpawnPrefab("stalker_atrium")
                        stalker.components.entitytracker:TrackEntity("stargate", stargate)
                        stargate:TrackStalker(stalker)
                    else
                        stalker = _G.SpawnPrefab("stalker")
                    end
                    local x, y, z = inst.Transform:GetWorldPosition()
                    local rot = inst.Transform:GetRotation()
                    inst:Remove()

                    stalker.Transform:SetPosition(x, y, z)
                    stalker.Transform:SetRotation(rot)
                    stalker.sg:GoToState("resurrect")

                    giver.components.sanity:DoDelta(_TUNING.REVIVE_SHADOW_SANITY_PENALTY)
                end
            end
            inst.components.trader.onaccept = OnAccept
            inst.components.trader:SetAbleToAcceptTest(ItemTradeTest)
        end
    )
end
