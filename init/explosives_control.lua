AddComponentPostInit("explosive", function(explosive, inst)
    inst.buildingdamage = 0
    explosive.hk_previous_OnBurnt = explosive.OnBurnt

    function explosive:OnBurnt()
        local x, y, z = self.inst.Transform:GetWorldPosition()
        local entities_within_range = GLOBAL.TheSim:FindEntities(x, y, z, explosive.explosiverange, nil, {
            "INLIMBO"
        })
        local structures_within_range = {}
        local player_structure_nearby = false

        for _, v in ipairs(entities_within_range) do
            if v.components.burnable ~= nil and not v.components.burnable:IsBurning() then
                if v:HasTag("structure") and not non_player_structures[v.prefab] then
                    player_structure_nearby = true
                end

                table.insert(structures_within_range, v)
            end
        end

        explosive:hk_previous_OnBurnt()

        if player_structure_nearby then
            for _, v in ipairs(structures_within_range) do
                if v:IsValid() and not v:IsInLimbo() and v.components.burnable ~= nil and
                    v.components.burnable:IsBurning() then
                    v.components.burnable:Extinguish(true, 100)
                end
            end
        end
    end
end)

log_i("Explosives restricted to protect against player structures damage")
