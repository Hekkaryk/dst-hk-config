observer_filenames = {}
observer_filenames.observed = "hk_config_observed.txt"
observer_filenames.announced = "hk_config_announced.txt"

local function what(inst, announce)
    local result = inst.name or "Unidentified entity without a name"
    return (inst.GUID and not announce) and result .. "[" .. inst.GUID .. "]" or result
end
local function who(inst, data)
    local result = data.attacker.name
    return data.attacker.userid and result .. " " .. data.attacker.userid or result
end
-- log only
local function lasthit_log(inst, data)
    log_l(what(inst) .. " was killed by " .. who(inst, data))
end
local function kill_log(inst)
    inst:ListenForEvent("attacked", lasthit_log)
    log_l(what(inst) .. " died")
end
-- log & announce
local function lasthit_ann(inst, data)
    log_l(what(inst) .. " was killed by " .. who(inst, data))
    announce(what(inst, true) .. " was killed by " .. data.attacker.name)
end
local function kill_ann(inst)
    inst:ListenForEvent("attacked", lasthit_ann)
    log_l(what(inst) .. " died")
end

local logged = {
    "beefalo",
    "glommer",
    "chester",
    "koalefant_summer",
    "koalefant_winter",
    "pigman",
    "bunnyman",
    "rocky",
    "hutch"
}
for k, v in pairs(logged) do
    AddPrefabPostInit(
        v,
        function(inst)
            inst:ListenForEvent("death", kill_log)
        end
    )
end
local announced = {    
    "bearger",
    "dragonfly", 
    "moose",
    "deerclops",
    "walrus",
    "spiderqueen",
    "beequeen",
    "antlion",
	"warg",
	"spat",
	"lightninggoat",
	"minotaur",
    "deer",
	"toadstool",
	"toadstool_dark",
	"stalker",
	"stalker_arium",
	"stalker_forest",
	"chester",
	"hutch",
	"bishop",
	"knight",
	"rook",
	"shadow_bishop",
	"shadow_knight",
	"shadow_rook",
	"rook_nightmare",
	"knight_nightmare",
	"bishop_nightmare",
	"pigguard",
	"leif",
    "leif_sparse",
    "antlion"
}
for k, v in pairs(announced) do
    AddPrefabPostInit(
        v,
        function(inst)
            inst:ListenForEvent("death", kill_ann)
        end
    )
end
for _, v in pairs(observer_filenames) do
    if not file_check(v) then
        if file_create(v) then
            log_l("File " .. v .. " creted")
        else
            log_e("Could not create file " .. v .. " - cleanup might fail to work properly")
        end
    end
end
local data = {}

local function reload_data()
    data.observed = io.open(observer_filenames.observed, "rb")
    if data.observed then
        for line in data.observed:lines() do
            --local fixed_line = line:gsub("\r", "")
            for part in string.gmatch(line, "%S+") do
                if part and part ~= "" and part ~= nil then
                    AddPrefabPostInit(
                        part,
                        function(inst)
                            inst:ListenForEvent("death", kill_log)
                        end
                    )
                else
                    log_e("Corrupted entry found in file " .. observer_filenames.observed)
                end
            end
        end
    else
        log_e("Failed to open observed data")
    end

    data.announced = io.open(observer_filenames.announced, "rb")
    if data.announced then
        for line in data.announced:lines() do
            --local fixed_line = line:gsub("\r", "")
            for part in string.gmatch(line, "%S+") do
                if part and part ~= "" and part ~= nil then
                    table.insert(logged, part)
                    announced[part] = true
                    AddPrefabPostInit(
                        part,
                        function(inst)
                            inst:ListenForEvent("death", kill_ann)
                        end
                    )
                    log_l("Adding " .. part .. " to announced")
                else
                    log_e("Corrupted entry found in file " .. observer_filenames.announced)
                end
            end
        end
    else
        log_e("Failed to open announced data")
    end
end

reload_data()
