local function config_have(what, pre, su)
    local _pre = pre or ""
    local _su = su or ""
    if config[_pre .. what .. _su] then
        return what
    else
        return nil
    end
end
local function add_prefab(name)
    return config_have(name, "_add")
end

local prefabs_to_add = {}
prefabs_to_add.atrium_turret = add_prefab("atrium_turret")
prefabs_to_add.herder_turret = add_prefab("herder_turret")
prefabs_to_add.atrium_icebox = add_prefab("atrium_icebox")

PrefabFiles = {
    prefabs_to_add.atrium_turret,
    prefabs_to_add.herder_turret,
    prefabs_to_add.atrium_icebox
}

local recipe_ingredients = {}
local recipe_techs = {}

if prefabs_to_add.atrium_turret ~= nil then
    _STRINGS.NAMES.ATRIUM_TURRET = "Atrium Turret"
    _STRINGS.RECIPE_DESC.ATRIUM_TURRET = "Kills ALL hostile entities"
end
if prefabs_to_add.herder_turret ~= nil then
    _STRINGS.NAMES.HERDER_TURRET = "Herder Turret"
    _STRINGS.RECIPE_DESC.HERDER_TURRET = "Beefalo-friendly"
end
if prefabs_to_add.atrium_icebox ~= nil then
    _STRINGS.NAMES.ATRIUM_ICEBOX = "Atrium Icebox"
    _STRINGS.RECIPE_DESC.ATRIUM_ICEBOX = "Hammer-proof"
end

-- mod item recipes
function AddMod_Recipe(prefab, ingredients, tab, tech, description, image_path, placer_image_path)
    description = description or ". . ."
    image_path = image_path or ("images/inventoryimages/" .. prefab .. ".xml")
    placer_image_path = placer_image_path or nil
    local item_recipe = AddRecipe(prefab, ingredients, tab, tech, placer_image_path)
    item_recipe.atlas = _G.resolvefilepath(image_path)
    _STRINGS.RECIPE_DESC[string.upper(prefab)] = description
    return item_recipe
end

function Mod_Ingredient(prefab, count)
    return _Ingredient(prefab, count, "images/inventoryimages/" .. prefab .. ".xml")
end

function HelpAddRecipe(prefab, tab, tech)
    if recipe_ingredients[prefab] == nil then
        log_e("Recipe ingredients undefined for " .. prefab)
    else
        local _tech = tech
        if tech == nil and recipe_techs[prefab] == nil then
            log_d("Recipe tech undefined for " .. prefab .. ", using default (TECH.SCIENCE_TWO)")
        elseif tech == nil then
            tech = recipe_techs[prefab]
        end
        AddRecipe(prefab, recipe_ingredients[prefab], tab, _tech)
    end
end

---- recipes
-- monster drops
local recipes_monster_drops = config.recipes_monster_drops
if recipes_monster_drops ~= 0 then
    _STRINGS.RECIPE_DESC.SILK = "Durable and sticky"
    _STRINGS.RECIPE_DESC.PIGSKIN = "Dirty and tough"
    _STRINGS.RECIPE_DESC.BEEFALOWOOL = "Smelly and warm"
    _STRINGS.RECIPE_DESC.GEARS = "Magic and science united"
    _STRINGS.RECIPE_DESC.COONTAIL = "Feels weird"
    _STRINGS.RECIPE_DESC.LIVINGLOG = "You could swear it moved"
    _STRINGS.RECIPE_DESC.TENTACLESPOTS = "Velvet soft and cool"
    _STRINGS.RECIPE_DESC.BUTTER = "Colorful block of fat"
    _STRINGS.RECIPE_DESC.HORN = "Spiral beefalo hord"

    local shared_tech_level

    if recipes_monster_drops == 1 then
        shared_tech_level = recipe_tech.science_machine

        recipe_ingredients.silk = {_Ingredient("spidergland", 1)}
        recipe_ingredients.pigskin = {_Ingredient("meat", 1)}
        recipe_ingredients.beefalowool = {_Ingredient("meat", 1)}
        recipe_ingredients.gears = {_Ingredient("transistor", 1)}
        recipe_ingredients.coontail = {_Ingredient("meat", 1)}
        recipe_ingredients.livinglog = {_Ingredient("nightmarefuel", 1), _Ingredient("log", 1)}
        recipe_ingredients.tentaclespots = {_Ingredient("nightmarefuel", 1), _Ingredient("monstermeat", 1)}
        recipe_ingredients.butter = {_Ingredient("butterflywings", 4)}
        recipe_ingredients.horn = {_Ingredient("meat", 1), _Ingredient("boneshard", 4)}
    elseif recipes_monster_drops == 2 then
        shared_tech_level = recipe_tech.alchemy_engine

        recipe_ingredients.silk = {_Ingredient("monstermeat", 1), _Ingredient("spidergland", 1)}
        recipe_ingredients.pigskin = {_Ingredient("meat", 1), _Ingredient("monstermeat", 1)}
        recipe_ingredients.beefalowool = {_Ingredient("meat", 1), _Ingredient("cutgrass", 2)}
        recipe_ingredients.gears = {_Ingredient("nightmarefuel", 1), _Ingredient("transistor", 1)}
        recipe_ingredients.coontail = {_Ingredient("nightmarefuel", 1), _Ingredient("meat", 1)}
        recipe_ingredients.livinglog = {_Ingredient("nightmarefuel", 1), _Ingredient("log", 4)}
        recipe_ingredients.tentaclespots = {_Ingredient("nightmarefuel", 1), _Ingredient("monstermeat", 1)}
        recipe_ingredients.butter = {_Ingredient("butterflywings", 8)}
        recipe_ingredients.horn = {_Ingredient("nightmarefuel", 1), _Ingredient("meat", 1), _Ingredient("boneshard", 4)}
    elseif recipes_monster_drops == 3 then
        shared_tech_level = recipe_tech.prestihatitator

        recipe_ingredients.silk = {_Ingredient("nightmarefuel", 1), _Ingredient("monstermeat", 1), _Ingredient("spidergland", 1)}
        recipe_ingredients.pigskin = {_Ingredient("nightmarefuel", 1), _Ingredient("meat", 1), _Ingredient("monstermeat", 1)}
        recipe_ingredients.beefalowool = {_Ingredient("nightmarefuel", 1), _Ingredient("meat", 1), _Ingredient("cutgrass", 2)}
        recipe_ingredients.gears = {_Ingredient("nightmarefuel", 2), _Ingredient("transistor", 1)}
        recipe_ingredients.coontail = {_Ingredient("nightmarefuel", 2), _Ingredient("meat", 1)}
        recipe_ingredients.livinglog = {_Ingredient("nightmarefuel", 2), _Ingredient("log", 8)}
        recipe_ingredients.tentaclespots = {_Ingredient("nightmarefuel", 2), _Ingredient("monstermeat", 1)}
        recipe_ingredients.butter = {_Ingredient("nightmarefuel", 1), _Ingredient("butterflywings", 8)}
        recipe_ingredients.horn = {_Ingredient("nightmarefuel", 1), _Ingredient("meat", 1), _Ingredient("boneshard", 4)}
    end

    HelpAddRecipe("silk", _RECIPETABS.REFINE, shared_tech_level)
    HelpAddRecipe("pigskin", _RECIPETABS.REFINE, shared_tech_level)
    HelpAddRecipe("beefalowool", _RECIPETABS.REFINE, shared_tech_level)
    HelpAddRecipe("gears", _RECIPETABS.SCIENCE, shared_tech_level)
    HelpAddRecipe("coontail", _RECIPETABS.REFINE, shared_tech_level)
    HelpAddRecipe("livinglog", _RECIPETABS.REFINE, shared_tech_level)
    HelpAddRecipe("tentaclespots", _RECIPETABS.REFINE, shared_tech_level)
    HelpAddRecipe("butter", _RECIPETABS.FARM, shared_tech_level)
    HelpAddRecipe("horn", _RECIPETABS.REFINE, shared_tech_level)
end
-- plants
local recipes_plants = config.recipes_plants
if recipes_plants ~= 0 then
    _STRINGS.RECIPE_DESC.DUG_SAPLING = "Small sapling"
    _STRINGS.RECIPE_DESC.DUG_GRASS = "Small grass tuft"
    _STRINGS.RECIPE_DESC.DUG_BERRYBUSH = "Berry bush sapling"
    _STRINGS.RECIPE_DESC.DUG_BERRYBUSH2 = "Fancy berry bush sapling"
    _STRINGS.RECIPE_DESC.DUG_BERRYBUSH_JUICY = "Fancy berry bush sapling"
    if recipes_plants == 1 then
        AddRecipe(
            "dug_sapling",
            {_Ingredient("twigs", 3), _Ingredient("spoiled_food", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "dug_grass",
            {_Ingredient("cutgrass", 3), _Ingredient("spoiled_food", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "dug_berrybush",
            {_Ingredient("berries", 3), _Ingredient("spoiled_food", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "dug_berrybush2",
            {_Ingredient("berries", 3), _Ingredient("spoiled_food", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "dug_berrybush_juicy",
            {_Ingredient("berries_juicy", 3), _Ingredient("spoiled_food", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
    end
    if recipes_plants == 2 then
        AddRecipe("dug_sapling", {_Ingredient("twigs", 5), _Ingredient("poop", 1)}, _RECIPETABS.FARM, _TECH.SCIENCE_TWO)
        AddRecipe(
            "dug_grass",
            {_Ingredient("cutgrass", 5), _Ingredient("poop", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_TWO
        )
        AddRecipe(
            "dug_berrybush",
            {_Ingredient("berries", 5), _Ingredient("poop", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_TWO
        )
        AddRecipe(
            "dug_berrybush2",
            {_Ingredient("berries", 5), _Ingredient("poop", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_TWO
        )
        AddRecipe(
            "dug_berrybush_juicy",
            {_Ingredient("berries_juicy", 5), _Ingredient("poop", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_TWO
        )
    end
    if recipes_plants == 3 then
        AddRecipe(
            "dug_sapling",
            {_Ingredient("nightmarefuel", 1), _Ingredient("twigs", 5), _Ingredient("poop", 2)},
            _RECIPETABS.FARM,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "dug_grass",
            {_Ingredient("nightmarefuel", 1), _Ingredient("cutgrass", 5), _Ingredient("poop", 2)},
            _RECIPETABS.FARM,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "dug_berrybush",
            {_Ingredient("nightmarefuel", 1), _Ingredient("berries", 5), _Ingredient("poop", 2)},
            _RECIPETABS.FARM,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "dug_berrybush2",
            {_Ingredient("nightmarefuel", 1), _Ingredient("berries", 5), _Ingredient("poop", 2)},
            _RECIPETABS.FARM,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "dug_berrybush_juicy",
            {_Ingredient("nightmarefuel", 1), _Ingredient("berries_juicy", 5), _Ingredient("poop", 2)},
            _RECIPETABS.FARM,
            _TECH.MAGIC_ONE
        )
    end
end
-- animals
local recipes_animals = config.recipes_animals
if recipes_animals ~= 0 then
    _STRINGS.RECIPE_DESC.RABBIT = "Rabbits with antennaes?"
    _STRINGS.RECIPE_DESC.MOLE = "It's ugly and keeps moving"
    _STRINGS.RECIPE_DESC.BEE = "Sting like hornets"
    _STRINGS.RECIPE_DESC.CROW = "Omen of death..."
    _STRINGS.RECIPE_DESC.ROBIN = "Keeps trying to break free and fly away"
    _STRINGS.RECIPE_DESC.ROBIN_WINTER = "Pecks and hits with wings"
    if recipes_animals == 1 then
        AddRecipe(
            "rabbit",
            {_Ingredient("smallmeat", 1), _Ingredient("cutgrass", 2)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "mole",
            {_Ingredient("smallmeat", 1), _Ingredient("rocks", 2), _Ingredient("flint", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe("bee", {_Ingredient("honey", 1), _Ingredient("stinger", 2)}, _RECIPETABS.FARM, _TECH.SCIENCE_ONE)
        AddRecipe(
            "crow",
            {_Ingredient("smallmeat", 1), _Ingredient("feather_crow", 2)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "robin",
            {_Ingredient("smallmeat", 1), _Ingredient("feather_robin", 2)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "robin_winter",
            {_Ingredient("smallmeat", 1), _Ingredient("feather_robin_winter", 2)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "canary",
            {_Ingredient("smallmeat", 1), _Ingredient("feather_canary", 2)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
    end
    if recipes_animals == 2 then
        AddRecipe(
            "rabbit",
            {_Ingredient("smallmeat", 1), _Ingredient("cutgrass", 4)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_TWO
        )
        AddRecipe(
            "mole",
            {_Ingredient("smallmeat", 2), _Ingredient("rocks", 4), _Ingredient("flint", 2)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_TWO
        )
        AddRecipe("bee", {_Ingredient("honey", 1), _Ingredient("stinger", 2)}, _RECIPETABS.FARM, _TECH.SCIENCE_TWO)
        AddRecipe(
            "crow",
            {_Ingredient("smallmeat", 1), _Ingredient("feather_crow", 2)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_TWO
        )
        AddRecipe(
            "robin",
            {_Ingredient("smallmeat", 1), _Ingredient("feather_robin", 2)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_TWO
        )
        AddRecipe(
            "robin_winter",
            {_Ingredient("smallmeat", 1), _Ingredient("feather_robin_winter", 2)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_TWO
        )
        AddRecipe(
            "canary",
            {_Ingredient("smallmeat", 1), _Ingredient("feather_canary", 2)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_TWO
        )
    end
    if recipes_animals == 3 then
        AddRecipe(
            "rabbit",
            {_Ingredient("nightmarefuel", 1), _Ingredient("smallmeat", 2), _Ingredient("cutgrass", 2)},
            _RECIPETABS.FARM,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "mole",
            {
                _Ingredient("nightmarefuel", 1),
                _Ingredient("smallmeat", 4),
                _Ingredient("rocks", 4),
                _Ingredient("flint", 2)
            },
            _RECIPETABS.FARM,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "bee",
            {_Ingredient("nightmarefuel", 1), _Ingredient("honey", 2), _Ingredient("stinger", 2)},
            _RECIPETABS.FARM,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "crow",
            {_Ingredient("nightmarefuel", 1), _Ingredient("smallmeat", 2), _Ingredient("feather_crow", 2)},
            _RECIPETABS.FARM,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "robin",
            {_Ingredient("nightmarefuel", 1), _Ingredient("smallmeat", 2), _Ingredient("feather_robin", 2)},
            _RECIPETABS.FARM,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "robin_winter",
            {_Ingredient("nightmarefuel", 1), _Ingredient("smallmeat", 2), _Ingredient("feather_robin_winter", 2)},
            _RECIPETABS.FARM,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "canary",
            {_Ingredient("nightmarefuel", 1), _Ingredient("smallmeat", 2), _Ingredient("feather_canary", 2)},
            _RECIPETABS.FARM,
            _TECH.MAGIC_ONE
        )
    end
end
-- common
local recipes_common = config.recipes_common
if recipes_common ~= 0 then
    _STRINGS.RECIPE_DESC.BONESHARD = "Call the aid"
    _STRINGS.RECIPE_DESC.HOUNDSTOOTH = "The hunt continues"
    _STRINGS.RECIPE_DESC.HONEYCOMB = "Bees approve of this"
    _STRINGS.RECIPE_DESC.MARBLE = "Totally not a hoax"
    _STRINGS.RECIPE_DESC.LIGHTBULB = "Sorcery"
    _STRINGS.RECIPE_DESC.FIREFLIES = "Light please"
    if recipes_common == 1 then
        AddRecipe(
            "feather_crow",
            {_Ingredient("seeds", 3), _Ingredient("boneshard", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "feather_robin",
            {_Ingredient("seeds", 3), _Ingredient("boneshard", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "feather_robin_winter",
            {_Ingredient("seeds", 3), _Ingredient("boneshard", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "feather_canary",
            {_Ingredient("seeds", 3), _Ingredient("boneshard", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe("boneshard", {_Ingredient("smallmeat", 1)}, _RECIPETABS.REFINE, _TECH.SCIENCE_ONE)
        AddRecipe(
            "houndstooth",
            {_Ingredient("boneshard", 1), _Ingredient("smallmeat", 1)},
            _RECIPETABS.REFINE,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "honeycomb",
            {_Ingredient("twigs", 4), _Ingredient("honey", 1)},
            _RECIPETABS.REFINE,
            _TECH.SCIENCE_TWO
        )
        AddRecipe("marble", {_Ingredient("rocks", 3)}, _RECIPETABS.REFINE, _TECH.SCIENCE_ONE)
        AddRecipe(
            "lightbulb",
            {_Ingredient("torch", 1), _Ingredient("cutgrass", 1)},
            _RECIPETABS.LIGHT,
            _TECH.SCIENCE_ONE
        )
        AddRecipe("fireflies", {_Ingredient("lightbulb", 2)}, _RECIPETABS.LIGHT, _TECH.SCIENCE_ONE)
        AddRecipe("poop", {_Ingredient("cutgrass", 2)}, _RECIPETABS.REFINE, _TECH.SCIENCE_ONE)
        AddRecipe("spoiled_food", {_Ingredient("cutgrass", 1)}, _RECIPETABS.REFINE, _TECH.SCIENCE_ONE)
    end
    if recipes_common == 2 then
        AddRecipe(
            "feather_crow",
            {_Ingredient("seeds", 3), _Ingredient("boneshard", 2), _Ingredient("smallmeat", 2)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "feather_robin",
            {_Ingredient("seeds", 3), _Ingredient("boneshard", 2), _Ingredient("charcoal", 2)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "feather_robin_winter",
            {_Ingredient("seeds", 3), _Ingredient("boneshard", 2), _Ingredient("ice", 2)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "feather_canary",
            {_Ingredient("seeds", 3), _Ingredient("boneshard", 2), _Ingredient("goldnugget", 2)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe("boneshard", {_Ingredient("monstermeat", 1)}, _RECIPETABS.REFINE, _TECH.SCIENCE_TWO)
        AddRecipe(
            "houndstooth",
            {_Ingredient("boneshard", 3), _Ingredient("monstermeat", 1)},
            _RECIPETABS.REFINE,
            _TECH.SCIENCE_TWO
        )
        AddRecipe("honeycomb", {_Ingredient("log", 4), _Ingredient("honey", 2)}, _RECIPETABS.REFINE, _TECH.MAGIC_ONE)
        AddRecipe(
            "marble",
            {_Ingredient("cutstone", 1), _Ingredient("flint", 1)},
            _RECIPETABS.REFINE,
            _TECH.SCIENCE_TWO
        )
        AddRecipe("lightbulb", {_Ingredient("torch", 1), _Ingredient("log", 1)}, _RECIPETABS.LIGHT, _TECH.SCIENCE_TWO)
        AddRecipe(
            "fireflies",
            {_Ingredient("torch", 1), _Ingredient("lightbulb", 4)},
            _RECIPETABS.LIGHT,
            _TECH.SCIENCE_TWO
        )
        AddRecipe("poop", {_Ingredient("cutgrass", 4), _Ingredient("log", 1)}, _RECIPETABS.REFINE, _TECH.SCIENCE_TWO)
        AddRecipe("spoiled_food", {_Ingredient("cutgrass", 2)}, _RECIPETABS.REFINE, _TECH.SCIENCE_TWO)
    end
    if recipes_common == 3 then
        AddRecipe(
            "feather_crow",
            {_Ingredient("seeds", 3), _Ingredient("boneshard", 3), _Ingredient("nightmarefuel", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "feather_robin",
            {_Ingredient("seeds", 3), _Ingredient("boneshard", 3), _Ingredient("redgem", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "feather_robin_winter",
            {_Ingredient("seeds", 3), _Ingredient("boneshard", 3), _Ingredient("bluegem", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "feather_canary",
            {_Ingredient("seeds", 3), _Ingredient("boneshard", 3), _Ingredient("yellowgem", 1)},
            _RECIPETABS.FARM,
            _TECH.SCIENCE_ONE
        )
        AddRecipe("boneshard", {_Ingredient("meat", 1)}, _RECIPETABS.REFINE, _TECH.MAGIC_ONE)
        AddRecipe(
            "houndstooth",
            {_Ingredient("boneshard", 5), _Ingredient("meat", 1)},
            _RECIPETABS.REFINE,
            _TECH.MAGIC_ONE
        )
        AddRecipe("honeycomb", {_Ingredient("boards", 4), _Ingredient("honey", 4)}, _RECIPETABS.REFINE, _TECH.MAGIC_TWO)
        AddRecipe("marble", {_Ingredient("cutstone", 2)}, _RECIPETABS.REFINE, _TECH.MAGIC_ONE)
        AddRecipe(
            "lightbulb",
            {_Ingredient("torch", 1), _Ingredient("livinglog", 1)},
            _RECIPETABS.LIGHT,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "fireflies",
            {_Ingredient("nightmarefuel", 1), _Ingredient("lightbulb", 8)},
            _RECIPETABS.LIGHT,
            _TECH.MAGIC_ONE
        )
        AddRecipe("poop", {_Ingredient("nightmarefuel", 1), _Ingredient("log", 4)}, _RECIPETABS.REFINE, _TECH.MAGIC_ONE)
        AddRecipe(
            "spoiled_food",
            {_Ingredient("nightmarefuel", 1), _Ingredient("log", 2)},
            _RECIPETABS.REFINE,
            _TECH.MAGIC_ONE
        )
    end
end
-- spawners
local recipes_spawners = config.recipes_spawners
if recipes_spawners ~= 0 then
    if recipes_spawners == 1 then
        AddRecipe("lureplantbulb", {_Ingredient("smallmeat", 1)}, _RECIPETABS.REFINE, _TECH.SCIENCE_ONE)
        AddRecipe(
            "spidereggsack",
            {_Ingredient("silk", 3), _Ingredient("spidergland", 2), _Ingredient("monstermeat", 1)},
            _RECIPETABS.TOWN,
            _TECH.SCIENCE_ONE
        )
    end
    if recipes_spawners == 2 then
        AddRecipe("lureplantbulb", {_Ingredient("monstermeat", 3)}, _RECIPETABS.REFINE, _TECH.SCIENCE_TWO)
        AddRecipe(
            "spidereggsack",
            {_Ingredient("silk", 6), _Ingredient("spidergland", 4), _Ingredient("monstermeat", 5)},
            _RECIPETABS.TOWN,
            _TECH.SCIENCE_TWO
        )
    end
    if recipes_spawners == 3 then
        AddRecipe(
            "lureplantbulb",
            {_Ingredient("nightmarefuel", 1), _Ingredient("meat", 1)},
            _RECIPETABS.REFINE,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "spidereggsack",
            {_Ingredient("silk", 9), _Ingredient("spidergland", 8), _Ingredient("monstermeat", 10)},
            _RECIPETABS.TOWN,
            _TECH.MAGIC_ONE
        )
    end
end
-- rares
local recipes_rare = config.recipes_rare
if recipes_rare ~= 0 then
    _STRINGS.RECIPE_DESC.MANDRAKE = "Those used to kill with voice"
    _STRINGS.RECIPE_DESC.KRAMPUS_SACK = "To avoid hours of paintrain"
    if recipes_rare == 1 then
        AddRecipe(
            "mandrake",
            {_Ingredient("papyrus", 2), _Ingredient("livinglog", 1)},
            _RECIPETABS.MAGIC,
            _TECH.SCIENCE_TWO
        )
        AddRecipe(
            "krampus_sack",
            {_Ingredient("papyrus", 4), _Ingredient("gears", 2)},
            _RECIPETABS.SURVIVAL,
            _TECH.SCIENCE_TWO
        )
        AddRecipe(
            "spiderhat",
            {_Ingredient("silk", 2), _Ingredient("monstermeat", 5)},
            _RECIPETABS.DRESS,
            _TECH.SCIENCE_TWO
        )
        AddRecipe(
            "walrus_tusk",
            {_Ingredient("meat", 2), _Ingredient("walrushat", 1)},
            _RECIPETABS.DRESS,
            _TECH.SCIENCE_TWO
        )
        AddRecipe("walrushat", {_Ingredient("silk", 5), _Ingredient("fish", 3)}, _RECIPETABS.DRESS, _TECH.SCIENCE_TWO)
    end
    if recipes_rare == 2 then
        AddRecipe(
            "mandrake",
            {_Ingredient("nightmarefuel", 2), _Ingredient("livinglog", 1)},
            _RECIPETABS.MAGIC,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "krampus_sack",
            {_Ingredient("nightmarefuel", 3), _Ingredient("papyrus", 6), _Ingredient("gears", 2)},
            _RECIPETABS.SURVIVAL,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "spiderhat",
            {_Ingredient("silk", 6), _Ingredient("monstermeat", 5)},
            _RECIPETABS.DRESS,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "walrus_tusk",
            {_Ingredient("meat", 10), _Ingredient("walrushat", 1)},
            _RECIPETABS.DRESS,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "walrushat",
            {_Ingredient("meat", 2), _Ingredient("silk", 8), _Ingredient("fish", 5)},
            _RECIPETABS.DRESS,
            _TECH.MAGIC_ONE
        )
    end
    if recipes_rare == 3 then
        AddRecipe(
            "mandrake",
            {_Ingredient("nightmarefuel", 2), _Ingredient("livinglog", 1)},
            _RECIPETABS.MAGIC,
            _TECH.MAGIC_TWO
        )
        AddRecipe(
            "krampus_sack",
            {_Ingredient("nightmarefuel", 9), _Ingredient("papyrus", 8), _Ingredient("gears", 4)},
            _RECIPETABS.SURVIVAL,
            _TECH.MAGIC_TWO
        )
        AddRecipe(
            "spiderhat",
            {_Ingredient("silk", 12), _Ingredient("monstermeat", 5)},
            _RECIPETABS.DRESS,
            _TECH.MAGIC_TWO
        )
        AddRecipe(
            "walrus_tusk",
            {_Ingredient("meat", 20), _Ingredient("walrushat", 1)},
            _RECIPETABS.DRESS,
            _TECH.MAGIC_TWO
        )
        AddRecipe(
            "walrushat",
            {_Ingredient("nightmarefuel", 2), _Ingredient("silk", 12), _Ingredient("fish", 7)},
            _RECIPETABS.DRESS,
            _TECH.MAGIC_TWO
        )
    end
end
-- combat
local recipes_combat = config.recipes_combat
if recipes_combat ~= 0 then
    _STRINGS.RECIPE_DESC.TENTACLESPIKE = "Do not swallow"
    if recipes_combat == 1 then
        AddRecipe(
            "wathgrithrhat",
            {_Ingredient("goldnugget", 1), _Ingredient("cutstone", 1)},
            _RECIPETABS.WAR,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "spear_wathgrithr",
            {_Ingredient("flint", 3), _Ingredient("rope", 1), _Ingredient("goldnugget", 1)},
            _RECIPETABS.WAR,
            _TECH.SCIENCE_ONE
        )
        AddRecipe(
            "tentaclespike",
            {_Ingredient("log", 1), _Ingredient("stinger", 3), _Ingredient("monstermeat", 1)},
            _RECIPETABS.WAR,
            _TECH.SCIENCE_ONE
        )
    end
    if recipes_combat == 2 then
        AddRecipe(
            "wathgrithrhat",
            {_Ingredient("goldnugget", 5), _Ingredient("cutstone", 3)},
            _RECIPETABS.WAR,
            _TECH.SCIENCE_TWO
        )
        AddRecipe(
            "spear_wathgrithr",
            {_Ingredient("flint", 4), _Ingredient("rope", 2), _Ingredient("goldnugget", 2)},
            _RECIPETABS.WAR,
            _TECH.SCIENCE_TWO
        )
        AddRecipe(
            "tentaclespike",
            {_Ingredient("boards", 1), _Ingredient("stinger", 3), _Ingredient("monstermeat", 2)},
            _RECIPETABS.WAR,
            _TECH.SCIENCE_TWO
        )
    end
    if recipes_combat == 3 then
        AddRecipe(
            "wathgrithrhat",
            {_Ingredient("goldnugget", 1), _Ingredient("cutstone", 1)},
            _RECIPETABS.WAR,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "spear_wathgrithr",
            {_Ingredient("flint", 5), _Ingredient("rope", 3), _Ingredient("goldnugget", 1)},
            _RECIPETABS.WAR,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "tentaclespike",
            {_Ingredient("livinglog", 1), _Ingredient("stinger", 3), _Ingredient("monstermeat", 3)},
            _RECIPETABS.WAR,
            _TECH.MAGIC_ONE
        )
    end
end
-- caves
local recipes_caves = config.recipes_caves
if recipes_caves ~= 0 then
    _STRINGS.RECIPE_DESC.THULECITE_PIECES = "Feel cold and greasy"
    _STRINGS.RECIPE_DESC.SLURPER_PELT = "Reeks of dead things"
    _STRINGS.RECIPE_DESC.MANRABBIT_TAIL = "Create your own legion of Bunnyman"
    if recipes_caves == 1 then
        AddRecipe(
            "thulecite_pieces",
            {_Ingredient("nightmarefuel", 1), _Ingredient("goldnugget", 1)},
            _RECIPETABS.MAGIC,
            _TECH.MAGIC_ONE,
            nil,
            nil,
            nil,
            6,
            nil
        )
        AddRecipe(
            "slurper_pelt",
            {_Ingredient("nightmarefuel", 1), _Ingredient("monstermeat_dried", 1)},
            _RECIPETABS.MAGIC,
            _TECH.MAGIC_ONE
        )
        AddRecipe(
            "manrabbit_tail",
            {_Ingredient("nightmarefuel", 1), _Ingredient("rabbit", 1)},
            _RECIPETABS.MAGIC,
            _TECH.MAGIC_ONE
        )
    elseif recipes_caves == 2 then
        AddRecipe(
            "thulecite_pieces",
            {_Ingredient("nightmarefuel", 5), _Ingredient("goldnugget", 1), _Ingredient("cutstone", 1)},
            _RECIPETABS.MAGIC,
            _TECH.MAGIC_TWO,
            nil,
            nil,
            nil,
            6,
            nil
        )
        AddRecipe(
            "slurper_pelt",
            {_Ingredient("nightmarefuel", 1), _Ingredient("monstermeat_dried", 3)},
            _RECIPETABS.MAGIC,
            _TECH.MAGIC_TWO
        )
        AddRecipe(
            "manrabbit_tail",
            {_Ingredient("nightmarefuel", 1), _Ingredient("rabbit", 3)},
            _RECIPETABS.MAGIC,
            _TECH.MAGIC_TWO
        )
    end
    if recipes_caves == 3 then
        AddRecipe(
            "thulecite_pieces",
            {_Ingredient("nightmarefuel", 10), _Ingredient("goldnugget", 5), _Ingredient("cutstone", 5)},
            _RECIPETABS.MAGIC,
            _TECH.MAGIC_TWO,
            nil,
            nil,
            nil,
            6,
            nil
        )
        AddRecipe(
            "slurper_pelt",
            {_Ingredient("nightmarefuel", 5), _Ingredient("monstermeat_dried", 5)},
            _RECIPETABS.MAGIC,
            _TECH.MAGIC_TWO
        )
        AddRecipe(
            "manrabbit_tail",
            {_Ingredient("nightmarefuel", 5), _Ingredient("rabbit", 5)},
            _RECIPETABS.MAGIC,
            _TECH.MAGIC_TWO
        )
    end
end
-- Thulecite optional recipe
if config["recipes_thulecite"] ~= 0 then
    _STRINGS.RECIPE_DESC.THULECITE = "Feels cold and greasy"
    if config["recipes_thulecite"] == 1 then
        AddRecipe(
            "thulecite",
            {_Ingredient("nightmarefuel", 1), _Ingredient("goldnugget", 1)},
            _RECIPETABS.MAGIC,
            _TECH.MAGIC_ONE
        )
    elseif config["recipes_thulecite"] == 2 then
        AddRecipe(
            "thulecite",
            {_Ingredient("nightmarefuel", 5), _Ingredient("goldnugget", 1), _Ingredient("cutstone", 1)},
            _RECIPETABS.MAGIC,
            _TECH.MAGIC_TWO
        )
    elseif config["recipes_thulecite"] == 3 then
        AddRecipe(
            "thulecite",
            {_Ingredient("nightmarefuel", 10), _Ingredient("goldnugget", 5), _Ingredient("cutstone", 5)},
            _RECIPETABS.MAGIC,
            _TECH.MAGIC_TWO
        )
    end
end
-- chesspieces
local recipes_chess = config.recipes_chess
if recipes_chess ~= 0 then
    _STRINGS.RECIPE_DESC.CHESSPIECE_ROOK_SKETCH = "Rook Figure sketch"
    _STRINGS.RECIPE_DESC.CHESSPIECE_KNIGHT_SKETCH = "Knight Figure sketch"
    _STRINGS.RECIPE_DESC.CHESSPIECE_BISHOP_SKETCH = "Bishop Figure sketch"
    _STRINGS.RECIPE_DESC.CHESSPIECE_FORMAL_SKETCH = "Kingly Figure sketch"
    _STRINGS.RECIPE_DESC.CHESSPIECE_MUSE_SKETCH = "Queenly Figure sketch"
    _STRINGS.RECIPE_DESC.CHESSPIECE_PAWN_SKETCH = "Pawn Figure sketch"
    if recipes_chess == 1 then
        AddRecipe(
            "chesspiece_rook_sketch",
            {_Ingredient("papyrus", 1), _Ingredient("charcoal", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_knight_sketch",
            {_Ingredient("papyrus", 1), _Ingredient("charcoal", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_bishop_sketch",
            {_Ingredient("papyrus", 1), _Ingredient("charcoal", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_formal_sketch",
            {_Ingredient("papyrus", 1), _Ingredient("charcoal", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_muse_sketch",
            {_Ingredient("papyrus", 1), _Ingredient("charcoal", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_pawn_sketch",
            {_Ingredient("papyrus", 1), _Ingredient("charcoal", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
    end
    if recipes_chess == 2 then
        AddRecipe(
            "chesspiece_rook_sketch",
            {_Ingredient("nightmarefuel", 1), _Ingredient("papyrus", 2), _Ingredient("featherpencil", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_knight_sketch",
            {_Ingredient("nightmarefuel", 1), _Ingredient("papyrus", 2), _Ingredient("featherpencil", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_bishop_sketch",
            {_Ingredient("nightmarefuel", 1), _Ingredient("papyrus", 2), _Ingredient("featherpencil", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_formal_sketch",
            {_Ingredient("nightmarefuel", 1), _Ingredient("papyrus", 2), _Ingredient("featherpencil", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_muse_sketch",
            {_Ingredient("nightmarefuel", 1), _Ingredient("papyrus", 2), _Ingredient("featherpencil", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_pawn_sketch",
            {_Ingredient("nightmarefuel", 1), _Ingredient("papyrus", 2), _Ingredient("featherpencil", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
    end
    if recipes_chess == 3 then
        AddRecipe(
            "chesspiece_rook_sketch",
            {_Ingredient("nightmarefuel", 3), _Ingredient("papyrus", 3), _Ingredient("featherpencil", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_knight_sketch",
            {_Ingredient("nightmarefuel", 3), _Ingredient("papyrus", 3), _Ingredient("featherpencil", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_bishop_sketch",
            {_Ingredient("nightmarefuel", 3), _Ingredient("papyrus", 3), _Ingredient("featherpencil", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_formal_sketch",
            {_Ingredient("nightmarefuel", 3), _Ingredient("papyrus", 3), _Ingredient("featherpencil", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_muse_sketch",
            {_Ingredient("nightmarefuel", 3), _Ingredient("papyrus", 3), _Ingredient("featherpencil", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
        AddRecipe(
            "chesspiece_pawn_sketch",
            {_Ingredient("nightmarefuel", 3), _Ingredient("papyrus", 3), _Ingredient("featherpencil", 1)},
            _RECIPETABS.SCULPTING,
            _TECH.LOST
        )
    end
end

-- Mutli-World recipes
local recipes_multiworld = config.recipes_multiworld
if recipes_multiworld ~= 0 then
    if loaded_mods["Multi-Worlds DST"] then
        if recipes_multiworld == 1 then
            AddMod_Recipe(
                "coconut",
                {_Ingredient("acorn", 1), _Ingredient("log", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "dug_bambootree",
                {_Ingredient("twigs", 4), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "dug_coffeebush",
                {_Ingredient("ash", 4), _Ingredient("seeds", 1), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "dug_bittersweetbush",
                {_Ingredient("nitre", 1), _Ingredient("berries", 1), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "dug_mintybush",
                {_Ingredient("ice", 4), _Ingredient("berries", 1), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "jungletreeseed",
                {_Ingredient("twiggy_nut", 1), _Ingredient("pinecone", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "mucus",
                {_Ingredient("nitre", 1), _Ingredient("monstermeat", 1)},
                _RECIPETABS.REFINE,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "venom_gland",
                {_Ingredient("nitre", 1), _Ingredient("spidergland", 1)},
                _RECIPETABS.REFINE,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "sand",
                {_Ingredient("rocks", 1), _Ingredient("flint", 1)},
                _RECIPETABS.REFINE,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "seashell",
                {_Ingredient("smallmeat", 1), _Ingredient("nitre", 1)},
                _RECIPETABS.REFINE,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "snakeskin",
                {_Ingredient("monstermeat", 1), _Ingredient("pigskin", 1)},
                _RECIPETABS.REFINE,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "turf_snowy",
                {_Ingredient("ice", 2), _Ingredient("cutgrass", 5)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "turf_slimey",
                {_Ingredient("monstermeat", 2), _Ingredient("cutgrass", 5)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "turf_ice_lake",
                {_Ingredient("ice", 4), _Ingredient("nitre", 1)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "turf_grass_gray",
                {_Ingredient("ash", 2), _Ingredient("cutgrass", 1)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "turf_ash",
                {_Ingredient("ash", 4), _Ingredient("rocks", 1)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "turf_volcano",
                {_Ingredient("ash", 4), _Ingredient("charcoal", 1)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "turf_magma",
                {_Ingredient("charcoal", 4), _Ingredient("rocks", 1)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "turf_lava_rock",
                {_Ingredient("charcoal", 4), _Ingredient("rocks", 2)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "turf_grass_blue",
                {_Ingredient("nitre", 4), _Ingredient("cutgrass", 4)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe("turf_sand", {Mod_Ingredient("sand", 4)}, _RECIPETABS.TOWN, _TECH.SCIENCE_ONE)
            AddMod_Recipe("turf_jungle", {Mod_Ingredient("jungletreeseed", 1)}, _RECIPETABS.TOWN, _TECH.SCIENCE_ONE)
        elseif recipes_multiworld == 2 then
            AddMod_Recipe(
                "coconut",
                {_Ingredient("acorn", 1), _Ingredient("log", 4)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "dug_bambootree",
                {_Ingredient("twigs", 12), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "dug_coffeebush",
                {_Ingredient("ash", 8), _Ingredient("seeds", 3), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "dug_bittersweetbush",
                {_Ingredient("nitre", 3), _Ingredient("berries", 3), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "dug_mintybush",
                {_Ingredient("ice", 8), _Ingredient("berries", 3), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "jungletreeseed",
                {_Ingredient("twiggy_nut", 1), _Ingredient("pinecone", 3)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "mucus",
                {_Ingredient("nitre", 4), _Ingredient("monstermeat", 1)},
                _RECIPETABS.REFINE,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "venom_gland",
                {_Ingredient("nitre", 4), _Ingredient("spidergland", 1)},
                _RECIPETABS.REFINE,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "sand",
                {_Ingredient("rocks", 2), _Ingredient("flint", 1)},
                _RECIPETABS.REFINE,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "seashell",
                {_Ingredient("smallmeat", 3), _Ingredient("nitre", 1)},
                _RECIPETABS.REFINE,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "snakeskin",
                {_Ingredient("monstermeat", 2), _Ingredient("pigskin", 1)},
                _RECIPETABS.REFINE,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "turf_snowy",
                {_Ingredient("ice", 4), _Ingredient("cutgrass", 5)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "turf_slimey",
                {_Ingredient("monstermeat", 4), _Ingredient("cutgrass", 5)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "turf_ice_lake",
                {_Ingredient("ice", 8), _Ingredient("nitre", 1)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "turf_grass_gray",
                {_Ingredient("ash", 4), _Ingredient("cutgrass", 2)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "turf_ash",
                {_Ingredient("ash", 8), _Ingredient("rocks", 1)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "turf_volcano",
                {_Ingredient("ash", 8), _Ingredient("charcoal", 1)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "turf_magma",
                {_Ingredient("charcoal", 8), _Ingredient("rocks", 1)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "turf_lava_rock",
                {_Ingredient("charcoal", 8), _Ingredient("rocks", 2)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "turf_grass_blue",
                {_Ingredient("nitre", 8), _Ingredient("cutgrass", 4)},
                _RECIPETABS.TOWN,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe("turf_sand", {Mod_Ingredient("sand", 8)}, _RECIPETABS.TOWN, _TECH.SCIENCE_TWO)
            AddMod_Recipe("turf_jungle", {Mod_Ingredient("jungletreeseed", 4)}, _RECIPETABS.TOWN, _TECH.SCIENCE_TWO)
        elseif recipes_multiworld == 3 then
            AddMod_Recipe(
                "coconut",
                {_Ingredient("nightmarefuel", 1), _Ingredient("acorn", 2), _Ingredient("log", 4)},
                _RECIPETABS.FARM,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "dug_bambootree",
                {_Ingredient("nightmarefuel", 1), _Ingredient("sapling", 1), _Ingredient("twigs", 4)},
                _RECIPETABS.FARM,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "dug_coffeebush",
                {
                    _Ingredient("nightmarefuel", 1),
                    _Ingredient("dug_berrybush_juicy", 1),
                    _Ingredient("ash", 8),
                    _Ingredient("poop", 2)
                },
                _RECIPETABS.FARM,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "dug_bittersweetbush",
                {
                    _Ingredient("nightmarefuel", 1),
                    _Ingredient("nitre", 4),
                    _Ingredient("dug_berrybush", 1),
                    _Ingredient("poop", 2)
                },
                _RECIPETABS.FARM,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "dug_mintybush",
                {
                    _Ingredient("nightmarefuel", 1),
                    _Ingredient("ice", 8),
                    _Ingredient("dug_berrybush2", 1),
                    _Ingredient("poop", 2)
                },
                _RECIPETABS.FARM,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "jungletreeseed",
                {_Ingredient("nightmarefuel", 1), _Ingredient("twiggy_nut", 4), _Ingredient("pinecone", 4)},
                _RECIPETABS.FARM,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "mucus",
                {_Ingredient("nightmarefuel", 2), _Ingredient("nitre", 4), _Ingredient("monstermeat", 4)},
                _RECIPETABS.REFINE,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "venom_gland",
                {_Ingredient("nightmarefuel", 2), _Ingredient("nitre", 4), _Ingredient("spidergland", 4)},
                _RECIPETABS.REFINE,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "sand",
                {_Ingredient("nightmarefuel", 1), _Ingredient("rocks", 4), _Ingredient("flint", 4)},
                _RECIPETABS.REFINE,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "seashell",
                {_Ingredient("nightmarefuel", 2), _Ingredient("smallmeat", 4), _Ingredient("nitre", 4)},
                _RECIPETABS.REFINE,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "snakeskin",
                {_Ingredient("nightmarefuel", 2), _Ingredient("monstermeat", 4), _Ingredient("pigskin", 2)},
                _RECIPETABS.REFINE,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "turf_snowy",
                {_Ingredient("nightmarefuel", 1), _Ingredient("ice", 8), _Ingredient("cutgrass", 5)},
                _RECIPETABS.TOWN,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "turf_slimey",
                {_Ingredient("nightmarefuel", 1), _Ingredient("monstermeat", 4), _Ingredient("cutgrass", 5)},
                _RECIPETABS.TOWN,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "turf_ice_lake",
                {_Ingredient("nightmarefuel", 1), _Ingredient("ice", 8), _Ingredient("nitre", 4)},
                _RECIPETABS.TOWN,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "turf_grass_gray",
                {_Ingredient("nightmarefuel", 1), _Ingredient("ash", 8), _Ingredient("cutgrass", 4)},
                _RECIPETABS.TOWN,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "turf_ash",
                {_Ingredient("nightmarefuel", 1), _Ingredient("ash", 8), _Ingredient("rocks", 4)},
                _RECIPETABS.TOWN,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "turf_volcano",
                {_Ingredient("nightmarefuel", 1), _Ingredient("ash", 8), _Ingredient("charcoal", 4)},
                _RECIPETABS.TOWN,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "turf_magma",
                {_Ingredient("nightmarefuel", 1), _Ingredient("charcoal", 8), _Ingredient("rocks", 4)},
                _RECIPETABS.TOWN,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "turf_lava_rock",
                {_Ingredient("nightmarefuel", 1), _Ingredient("charcoal", 8), _Ingredient("rocks", 4)},
                _RECIPETABS.TOWN,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "turf_grass_blue",
                {_Ingredient("nightmarefuel", 1), _Ingredient("nitre", 8), _Ingredient("cutgrass", 8)},
                _RECIPETABS.TOWN,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "turf_sand",
                {_Ingredient("nightmarefuel", 1), Mod_Ingredient("sand", 12)},
                _RECIPETABS.TOWN,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "turf_jungle",
                {_Ingredient("nightmarefuel", 1), Mod_Ingredient("jungletreeseed", 4)},
                _RECIPETABS.TOWN,
                _TECH.MAGIC_ONE
            )
        end
        _STRINGS.NAMES.TURF_SNOWY = "Snowy turf"
        _STRINGS.NAMES.TURF_SLIMEY = "Slimey turf"
        _STRINGS.NAMES.TURF_ICE_LAKE = "Ice lake turf"
        _STRINGS.NAMES.TURF_GRASS_GRAY = "Gray grass turf"
        _STRINGS.NAMES.TURF_ASH = "Ash turf"
        _STRINGS.NAMES.TURF_VOLCANO = "Volcano turf"
        _STRINGS.NAMES.TURF_MAGMA = "Magma turf"
        _STRINGS.NAMES.TURF_LAVA_ROCK = "Lava rock turf"
        _STRINGS.NAMES.TURF_GRASS_BLUE = "Blue grass turf"
        _STRINGS.NAMES.TURF_SAND = "Sand turf"
        _STRINGS.NAMES.TURF_JUNGLE = "Jungle turf"
        _STRINGS.RECIPE_DESC.COCONUT = "Coconut"
        _STRINGS.RECIPE_DESC.DUG_BAMBOOTREE = "Bamboo plant"
        _STRINGS.RECIPE_DESC.DUG_COFFEEBUSH = "Coffee coffee coffee!"
        _STRINGS.RECIPE_DESC.DUG_BITTERSWEETBUSH = "Bittersweetbush"
        _STRINGS.RECIPE_DESC.DUG_MINTYBUSH = "Mintybush"
        _STRINGS.RECIPE_DESC.JUNGLETREESEED = "Jungle tree seed"
        _STRINGS.RECIPE_DESC.MUCUS = "Mucus"
        _STRINGS.RECIPE_DESC.VENOM_GLAND = "Used in antidote"
        _STRINGS.RECIPE_DESC.SAND = "Sand"
        _STRINGS.RECIPE_DESC.SEASHELL = "Seashell"
        _STRINGS.RECIPE_DESC.SNAKESKIN = "Snakeskin"
        _STRINGS.RECIPE_DESC.TURF_SNOWY = "Snowy turf"
        _STRINGS.RECIPE_DESC.TURF_SLIMEY = "Slimey turf"
        _STRINGS.RECIPE_DESC.TURF_ICE_LAKE = "Ice lake turf"
        _STRINGS.RECIPE_DESC.TURF_GRASS_GRAY = "Gray grass turf"
        _STRINGS.RECIPE_DESC.TURF_ASH = "Ash turf"
        _STRINGS.RECIPE_DESC.TURF_VOLCANO = "Volcano turf"
        _STRINGS.RECIPE_DESC.TURF_MAGMA = "Magma turf"
        _STRINGS.RECIPE_DESC.TURF_LAVA_ROCK = "Lava rock turf"
        _STRINGS.RECIPE_DESC.TURF_GRASS_BLUE = "Blue grass turf"
        _STRINGS.RECIPE_DESC.TURF_SAND = "Sand turf"
        _STRINGS.RECIPE_DESC.TURF_JUNGLE = "Jungle turf"
        log_i("Multi-World DST mod recipes added.")
    else
        log_e("Failed to add Multi-World DST mod recipes - Multi-World DST is missing.")
    end
end
local recipes_bulk_craft = config.recipes_bulk_craft
if recipes_bulk_craft then
    AddRecipe("cutstone", {_Ingredient("rocks", 15)}, _RECIPETABS.REFINE, _TECH.SCIENCE_ONE, nil, nil, nil, 5, nil)
    AddRecipe(
        "wall_stone_item",
        {_Ingredient("cutstone", 10)},
        _RECIPETABS.TOWN,
        _TECH.SCIENCE_ONE,
        nil,
        nil,
        nil,
        30,
        nil
    )
end

local recipes_berries_and_plants = config.recipes_berries_and_plants
if recipes_berries_and_plants ~= 0 then
    if
        loaded_mods["My Birds and Berries [Megarandom]"] or
            loaded_mods["Birds and Berries and Trees and Flowers for Friends"]
     then
        if recipes_berries_and_plants == 1 then
            AddMod_Recipe(
                "dug_berryblu2",
                {_Ingredient("blue_cap", 1), _Ingredient("berries", 1), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "dug_berryblue",
                {_Ingredient("blue_cap", 1), _Ingredient("berries", 1), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "dug_berrygre2",
                {_Ingredient("green_cap", 1), _Ingredient("berries", 1), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "dug_berrygree",
                {_Ingredient("green_cap", 1), _Ingredient("berries", 1), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "dug_pineapple",
                {_Ingredient("acorn", 1), _Ingredient("pinecone", 1), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_ONE
            )
            AddMod_Recipe(
                "treeapple",
                {_Ingredient("acorn", 1), _Ingredient("log", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_ONE
            )
        elseif recipes_berries_and_plants == 2 then
            AddMod_Recipe(
                "dug_berryblu2",
                {_Ingredient("blue_cap", 1), _Ingredient("berries", 3), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "dug_berryblue",
                {_Ingredient("blue_cap", 1), _Ingredient("berries", 3), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "dug_berrygre2",
                {_Ingredient("green_cap", 1), _Ingredient("berries", 3), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "dug_berrygree",
                {_Ingredient("green_cap", 1), _Ingredient("berries", 3), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "dug_pineapple",
                {_Ingredient("acorn", 3), _Ingredient("pinecone", 1), _Ingredient("poop", 1)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_TWO
            )
            AddMod_Recipe(
                "treeapple",
                {_Ingredient("acorn", 1), _Ingredient("log", 4)},
                _RECIPETABS.FARM,
                _TECH.SCIENCE_TWO
            )
        elseif recipes_berries_and_plants == 3 then
            AddMod_Recipe(
                "dug_berryblu2",
                {_Ingredient("nightmarefuel", 1), _Ingredient("berries", 5), _Ingredient("poop", 4)},
                _RECIPETABS.FARM,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "dug_berryblue",
                {_Ingredient("nightmarefuel", 1), _Ingredient("berries", 5), _Ingredient("poop", 4)},
                _RECIPETABS.FARM,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "dug_berrygre2",
                {_Ingredient("nightmarefuel", 1), _Ingredient("berries", 5), _Ingredient("poop", 4)},
                _RECIPETABS.FARM,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "dug_berrygree",
                {_Ingredient("nightmarefuel", 1), _Ingredient("berries", 5), _Ingredient("poop", 4)},
                _RECIPETABS.FARM,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "dug_pineapple",
                {_Ingredient("nightmarefuel", 1), _Ingredient("acorn", 6), _Ingredient("pinecone", 4)},
                _RECIPETABS.FARM,
                _TECH.MAGIC_ONE
            )
            AddMod_Recipe(
                "treeapple",
                {_Ingredient("nightmarefuel", 1), _Ingredient("acorn", 2), _Ingredient("log", 4)},
                _RECIPETABS.FARM,
                _TECH.MAGIC_ONE
            )
        end
        _STRINGS.NAMES.DUG_BERRYBLU2 = "Blueberry"
        _STRINGS.NAMES.DUG_BERRYBLUE = "Blueberry"
        _STRINGS.NAMES.DUG_BERRYGRE2 = "Greenberry"
        _STRINGS.NAMES.DUG_BERRYGREE = "Greenberry"
        _STRINGS.NAMES.DUG_PINEAPPLE = "Ananas"
        _STRINGS.NAMES.TREEAPPLE = "Apple"
        _STRINGS.RECIPE_DESC.DUG_BERRYBLU2 = "Tasty and sweet"
        _STRINGS.RECIPE_DESC.DUG_BERRYBLUE = "Tasty and sweet"
        _STRINGS.RECIPE_DESC.DUG_BERRYGRE2 = "Extremely bitter"
        _STRINGS.RECIPE_DESC.DUG_BERRYGREE = "Extremely bitter"
        _STRINGS.RECIPE_DESC.DUG_PINEAPPLE = "Fresh and tasty"
        _STRINGS.RECIPE_DESC.TREEAPPLE = "Healthy"
        log_i('"Birds and Berries and Trees and Flowers for Friends" mod recipes added.')
    else
        log_e(
            'Failed to add "Birds and Berries and Trees and Flowers for Friends" mod recipes - neither "Birds and Berries and Trees and Flowers for Friends" nor "My Birds and Berries [Megarandom]" are loaded.'
        )
    end
end

local recipes = {}

local function define_recipe(prefab, init, tab, name, desc, ingredients, tech)
    if prefab == nil then
        log_e("Recipe definition lack prefab")
    else
        if init == nil or init == true then
            recipes[prefab] = {}
            recipes[prefab].tab = tab
            recipes[prefab].name = name
            recipes[prefab].desc = desc
            log_d("Recipe prepared: " .. prefab)
        end

        if init ~= true and recipes[prefab] ~= nil then
            recipes[prefab].ingredients = ingredients
            recipes[prefab].tech = tech
            log_d("Recipe ready: " .. prefab)
        elseif init == false then
            log_e("update_recipe shortwired; call init_recipe first!")
        end
    end
end

local function init_recipe(prefab, tab, name, desc)
    define_recipe(prefab, true, tab, name, desc)
end

local function update_recipe(prefab, ingredients, tech)
    define_recipe(prefab, false, nil, nil, nil, ingredients, tech)
end

local function add_recipes(prefabs)
    for _,prefab in pairs(prefabs) do
        -- gtfo with invalid type of data for prefab name - we want strings only
        if prefab and type(prefab) ~= "string" then
            log_e("Invalid type of data used for prefab name of recipe to add")
            return
        end
        -- we need ingredients - can't make those up
        if prefab and recipes[prefab] and recipes[prefab].ingredients then
            -- lets try to recover partial recipes...
            if recipes[prefab].tab == nil then
                recipes[prefab].tab = _RECIPETABS.REFINE
                log_w("Recipe missing tab, using RECIPETABS.REFINE: " .. prefab)
            end
            if recipes[prefab].tech == nil then                
                recipes[prefab].tech = _TECH.SCIENCE_TWO
                log_w("Recipe missing tech, using TECH.SCIENCE_TWO: " .. prefab)
            end
            if not recipes[prefab].name or not type(recipes[prefab].name) == "string" then
                recipes[prefab].name = prefab
                log_w("Recipe missing name, using: " .. prefab)
            end 
            if not recipes[prefab].desc or not type(recipes[prefab].desc) == "string" then
                recipes[prefab].desc = prefab
                log_w("Recipe missing description, using: " .. prefab)
            end
            -- ok, add/replace recipe now
            AddMod_Recipe(
                prefab,
                recipes[prefab].ingredients,
                recipes[prefab].tab,
                recipes[prefab].tech
            )

            _STRINGS.NAMES[string.upper(prefab)] = recipes[prefab].name
            _STRINGS.RECIPE_DESC[string.upper(prefab)] = recipes[prefab].desc
            log_i("Recipe added: " .. prefab)
        else
            log_e("Recipe invalid: " .. prefab)
        end
    end
end

-- Tea and Trees recipes
local recipes_tea_and_trees = config.recipes_tea_and_trees
if recipes_tea_and_trees ~= 0 then
    if loaded_mods["Tea and Trees"] then
        local prefabs = {"peach_pit", "mango_seeds"}
        
        init_recipe("peach_pit",_RECIPETABS.FARM,"Peach pit","Grow your own peaches!")
        init_recipe("mango_seeds",_RECIPETABS.FARM,"Mango seeds","Because life is good")

        if recipes_tea_and_trees == 1 then
            update_recipe("peach_pit", {_Ingredient("carrot", 3), _Ingredient("berries", 3)}, _TECH.SCIENCE_TWO)
            update_recipe("mango_seeds", {_Ingredient("carrot", 3), _Ingredient("berries", 3)}, _TECH.SCIENCE_TWO)
        elseif recipes_tea_and_trees == 2 then
            update_recipe("peach_pit", {_Ingredient("dug_berrybush", 2), _Ingredient("carrot", 6), _Ingredient("poop", 5)}, _TECH.MAGIC_TWO)
            update_recipe("mango_seeds", {_Ingredient("dug_berrybush", 2), _Ingredient("carrot", 6), _Ingredient("poop", 5)}, _TECH.MAGIC_TWO)
        else
            update_recipe("peach_pit", {_Ingredient("nightmarefuel", 1), Mod_Ingredient("peach", 5), _Ingredient("poop", 10)}, _TECH.MAGIC_THREE)
            update_recipe("mango_seeds", {_Ingredient("nightmarefuel", 1), Mod_Ingredient("mango", 5), _Ingredient("poop", 10)}, _TECH.MAGIC_THREE)
        end

        add_recipes(prefabs)

        log_i('"Tea and Trees" mod recipes added.')
    else
        log_e(
            'Failed to add "Tea and Trees" mod recipes - "Tea and Trees" is not loaded.'
        )
    end
end

function AddImageless_Recipe(prefab, ingredients, tab, tech, description)
    description = description or prefab
    local item_recipe = AddRecipe(prefab, ingredients, tab, tech)
    --_STRINGS.RECIPE_DESC[string.upper(prefab)] = description
    return item_recipe
end

if config.add_atrium_turret then
    AddImageless_Recipe("atrium_turret_item", {_Ingredient("log", 1)}, _RECIPETABS.WAR, _TECH.LOST, "Atrium Turret")
end

if config.add_herder_turret then
    AddImageless_Recipe("herder_turret_item", {_Ingredient("log", 1)}, _RECIPETABS.WAR, _TECH.LOST, "Herder Turret")
end

if config.add_atrium_icebox then
    AddImageless_Recipe(
        "atrium_icebox",
        {Ingredient("thulecite", 4), Ingredient("gears", 2), Ingredient("moonrocknugget", 4)},
        _RECIPETABS.FARM,
        _TECH.MAGIC_TWO,
        "Atrium Icebox"
    )
end
