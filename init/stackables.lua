if IsServer then
  if config.stackable_animals ~= 0 then
    local function makeAnimalStackable(inst)
      if inst ~= nil then
        if (inst.components.stackable == nil) then
          inst:AddComponent("stackable")
        end
        inst.components.inventoryitem:SetOnDroppedFn(
          function(_) --animal)
            inst.components.perishable:StopPerishing()
            inst.sg:GoToState("stunned")
            if inst.components.stackable then
              while inst.components.stackable:StackSize() > 1 do
                local item = inst.components.stackable:Get()
                if item then
                  if item.components.inventoryitem then
                    item.components.inventoryitem:OnDropped()
                  end
                  item.Physics:Teleport(inst.Transform:GetWorldPosition())
                end
              end
            end
          end
        )
      end
    end
    if (config.stackable_animals % 2) == 1 then --rabbits
      AddPrefabPostInit("rabbit", makeAnimalStackable)
    end
    if math.floor(config.stackable_animals / 2) % 2 == 1 then --birds
      AddPrefabPostInit("crow", makeAnimalStackable)
      AddPrefabPostInit("robin", makeAnimalStackable)
      AddPrefabPostInit("robin_winter", makeAnimalStackable)
      AddPrefabPostInit("canary", makeAnimalStackable)
    end
    if math.floor(config.stackable_animals / 4) % 2 == 1 then --moleworms
      AddPrefabPostInit("mole", makeAnimalStackable)
    end
  end
  -- if config.minotaurhorn_stackable then
  --   local function makeStackable(inst)
  --     if inst ~= nil then
  --       if (inst.components.stackable == nil) then
  --         inst:AddComponent("stackable")
  --       end
  --     end
  --   end
  --   AddPrefabPostInit("minotaurhorn", makeStackable)
  -- end
end
