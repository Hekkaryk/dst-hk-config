local MinotaurSpawner = Class(function(self, inst)
    self.inst = inst
	self.days_to_spawn = 16
	self.locations = {}
	self.inst:WatchWorldState("isnewmoon", function(inst, isnewmoon) self:OnNewMoon(isnewmoon) end)
end,
nil,
{
})

function MinotaurSpawner:FindMinotaur(x, z)
	return TheSim:FindEntities(x, 10, z, 64, {"minotaur"})[1]
end

function MinotaurSpawner:OnNewMoon(isnewmoon)
	for _,loc in pairs(self.locations) do
		if isnewmoon and not loc.spawned_this_newmoon then
			local minotaur = nil
			if loc.has_minotaur then
				minotaur = self:FindMinotaur(loc.x, loc.z)
			end
			self:SpawnMinotaur(loc)
			loc.spawned_this_newmoon = true
		elseif not isnewmoon then
			loc.spawned_this_newmoon = false
		end
	end
end

function MinotaurSpawner:SpawnMinotaur(loc)
	local minotaur = SpawnPrefab("minotaur")
	minotaur.Transform:SetPosition(loc.x, 0, loc.z)
	local fx = SpawnPrefab("statue_transition_2")
	if fx ~= nil then
		fx.Transform:SetPosition(loc.x, 0, loc.z)
		fx.Transform:SetScale(2, 2, 2)
	end
	fx = SpawnPrefab("statue_transition")
	if fx ~= nil then
		fx.Transform:SetPosition(loc.x, 0, loc.z)
		fx.Transform:SetScale(1.5, 1.5, 1.5)
	end
	loc.has_minotaur = true
	minotaur:ListenForEvent("onremove", function() loc.has_minotaur = false end)
	minotaur:ListenForEvent("death", function() loc.last_death = TheWorld.state.cycles end)
end

function MinotaurSpawner:RegisterLocation(id, x, z)
	self.locations[id] = {	x = x,
							z = z,
							last_death = TheWorld.state.cycles,
							has_minotaur = self:FindMinotaur(x, z) ~= nil,
							spawned_this_newmoon = true}
end

function MinotaurSpawner:OnSave()
	local data = { locations = {} }
	for id,location in pairs(self.locations) do
		table.insert(data.locations, {	id = id,
										x = location.x,
										z = location.z,
										last_death = location.last_death,
										has_minotaur = location.has_minotaur,
										spawned_this_newmoon = location.spawned_this_newmoon	})
	end
	return data
end

function MinotaurSpawner:OnLoad(data)
	self.inst:DoTaskInTime(1, function()
		if not data or not data.locations then return end
		for _,loc in pairs(data.locations) do
			if self.locations[loc.id] then
				self.locations[loc.id].last_death = loc.last_death
				self.locations[loc.id].spawned_this_newmoon = loc.spawned_this_newmoon
			end
		end
		self:OnNewMoon(TheWorld.state.isnewmoon)
	end)
end

return MinotaurSpawner